#!/usr/bin/env bash

set -o errexit -o noclobber -o nounset -o pipefail
shopt -s failglob inherit_errexit

cd "${RUNTIME_DIRECTORY:?}"

adb=(adb -s 64d9334c) # My current phone serial number, as listed in `adb devices -l`

"${adb[@]}" pull /sdcard/Android/data/net.osmand.plus/files/tracks /sdcard/Android/data/net.osmand.plus/files/favorites "${HOME}/documents/OSMAnd"

# shellcheck disable=SC2016
parallel --verbose git -C {} checkout-index --all --prefix="${PWD}"/'"$(basename "$(dirname "$(dirname {})")")"/' ::: "${HOME}"/{documents,graphics}/.git/..
if ! "${adb[@]}" shell << 'EOF'; then
set -o errexit -o noclobber -o nounset

cleanup() {
    rm -f -r "${test_dir}"
}
trap cleanup EXIT

test_dir="$(mktemp --directory --tmpdir=/sdcard symlink-test.XXXXXX)"

ln -fsv / "${test_dir}/deleteme"
EOF
    echo 'WARNING: /sdcard does not support symlinks. Skipping them.' >&2
    find . -type l -delete -print
fi

"${adb[@]}" push ./* /sdcard/
