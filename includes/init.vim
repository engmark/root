set mouse = ""

let g:mapleader = ' '

nnoremap <leader>u :UndotreeToggle<CR>

vnoremap <C-S-Up> :m '<-2<CR>gv=gv
vnoremap <C-S-Down> :m '>+1<CR>gv=gv
