[[ "$(git diff --staged | rg --count ^-)" -eq "$(git diff --staged | rg --count ^+)" ]] # verify sort
"$(nix-build --attr driverInteractive --no-out-link FILE)/bin/nixos-test-driver"
"$(nix-build --attr driver --no-out-link FILE)/bin/nixos-test-driver" # breakpoint
"$(nix-build --attr PACKAGE.fetch-deps)" # update dotnet package dependendencies
"$(nix-build --attr PACKAGE --no-out-link)"/bin/EXECUTABLE # build run
$(which sshd) -f FILE # check config
abcde -Q cddb -o flac # rip audio cd
acpi_listen # events power configuration
adb devices -l # list android
adb # help
adb pull "$(adb shell pm path "$(adb shell pm list packages | cut --delimiter=: --fields=2 | grep --ignore-case APPNAME)" | cut --delimiter=: --fields=2)" # download android apk
adb pull /sdcard/DCIM/Camera # download directory recursive android
adb push PATHS /sdcard/ # upload file directory recursive android
adb reboot bootloader # fastboot android
adb shell # android
adb shell logcat -T 10 # list follow android log
age-keygen --output "${HOME}/.config/sops/age/keys.txt" # encryption ssh sops
alias ls
align_image_stack -vl -a aligned ./*.jpg # graphics
alsamixer # audio input output volume
ansible-lint --fix # check
appimage-run PATH
apropos --exact COMMAND # help man
apropos REGEX # help man
argo --namespace=NAMESPACE list # workflow
argo --namespace=NAMESPACE submit --from=workflowtemplate/TEMPLATE_NAME --generate-name=PREFIX --parameter=NAME=VALUE --parameter-file=FILE --watch TEMPLATE_FILE # job task
arp -a # network list arp cache
aspell --master=mi dump master # list word dictionary
aws-azure-login --no-prompt # authenticate configure
aws cloudformation delete-stack --stack-name=STACK && aws cloudformation wait stack-delete-complete --stack-name=STACK
aws cloudformation describe-stacks --output=text --query="Stacks[].Outputs[].OutputValue" --stack-name=STACK # output
aws configure
aws eks list-clusters # kubernetes
aws eks update-kubeconfig --name=CLUSTER_NAME # kubernetes
aws iam list-roles --output=text --query="Roles[?starts_with(RoleName, 'PREFIX')].RoleName"
aws lambda invoke --function-name=FUNCTION --payload=PAYLOAD /dev/stdout
aws --no-sign-request s3api list-objects-v2 --bucket=BUCKET --query="Contents[?ends_with(Key, '.EXTENSION')].Size"
aws --no-sign-request s3 ls # list bucket
aws --no-sign-request s3 ls s3://BUCKET/ # list file
aws rds describe-db-snapshots --query="sort_by(DBSnapshots[?starts_with(DBSnapshotIdentifier,'PREFIX')], &SnapshotCreateTime)[-1]"
aws s3 sync --recursive SOURCE TARGET # file copy
aws secretsmanager get-secret-value --secret-id ID --query SecretString --output text | xclip # password clipboard
aws ssm get-parameter --name /cdk-bootstrap/hnb659fds/version --query Parameter.Value | jq --raw-output . # cdk bootstrap
aws stepfunctions describe-execution --execution-arn=ARN
aws stepfunctions list-executions --state-machine-arn=ARN | jq --raw-output '.executions[] | .executionArn'
aws stepfunctions list-state-machines | jq --raw-output '.stateMachines[] | .stateMachineArn'
aws sts get-caller-identity # user
basename "$(find -L /dev/disk/by-uuid -samefile /dev/DISK)" # disk uuid
basename PATH # file name
basename PATH.SUFFIX .SUFFIX # remove file name extension suffix
bc <<< '2+2' # calculator math
bc --mathlib <<< 'e(1)' # calculator math e power
bc <<< 'scale = 10; sqrt ( 2 )' # calculator math precision scale float decimal
bg # background job
bind -P | rg --fixed-strings ' can be found on ' | perl -pe 's/((?<!\\)(?:\\\\)*)\\C/\1Ctrl/g;s/((?<!\\)(?:\\\\)*)\\e/\1Esc,/g' # keyboard shortcuts
bind -p | rg --invert-match --regexp='^$' --regexp='^#' --regexp='self-insert$' | sed "s/\(.*\)/bind '\1'/" | tr --squeeze-repeats '\n' ';' # shortcuts code
black . # format python file
bluetoothctl
bsdtar --list --file /boot/initramfs-linux.img | less # print kernel image files
btop # monitor processes system processor cpu memory ram network disk storage
builtin # bash list
bundix --lock # ruby bundle nix
bunzip2 file.bz2 # compression bzip
cachix watch-exec CACHE COMMAND # upload download sync nix cache
cal "$(date +%G)" # calendar year
cal # calendar month
cargo build # rust
cargo init --bin project-name # rust project create
cargo test # rust build test
cat /dev/zero > /tmp/ramdisk.XXXXXXXXXX/full.log # fill ramdisk partition file tmpfs
cat /etc/papersize # print
cat /etc/*release # os
cat /proc/cmdline # boot command
cat /proc/cpuinfo # hardware
cat /proc/devices # hardware
cat /proc/filesystems # hardware
cat /proc/misc # hardware
cat /proc/stat # hardware
cat /proc/sys/dev/cdrom/info
cat /proc/sys/kernel/pid_max # process
cat /proc/vmstat # hardware
cd "$(mktemp --directory)" # create temporary directory
cd - # back
cdk --app cdk.out ls 'PATTERN' # list cloudformation stack glob
cdk bootstrap # aws
cdk deploy --all --require-approval=never # aws
cdk diff # aws
cdk list # aws stack
cdk synthesize # aws template
chars 😂
chfn --full-name "John Doe"
chmod go= ~/.ssh/id_rsa # permission
chmod u+x PATH # permission executable
clinfo # opencl hardware list platform device
cmp --print-bytes "$(which arch)" "$(which uname)" # binary diff
comm -23 --nocheck-order <(alias -p) <(bash -lc 'alias -p') # list session aliases
COMMAND 2> >(grep --fixed-strings --invert-match --regex=REGEX >&2) # clean stderr
command_not_found_handle COMMAND # find locate command package
convert -density 150 -quality 100 input.pdf output.jpg # graphics extract image
convert ./*.jpg output.pdf # graphics combine image
convert ./name-*.gif name-%04d.png # format number graphics
coredumpctl dump PID > coredump.bin # trace segfault
coredumpctl gdb PID <<< bt # trace segfault
coredumpctl list # segfault
count . # filesystem
coverage html # python test coverage
coverage report # python test coverage
coverage run --module MODULE discover # python pytest unittest coverage
cp --archive SOURCE TARGET # recursive owner permissions
cronlist
cronlist --system
cronlist --to '23:59:59'
csplit --prefix header- --suffix-format %02d.txt --elide-empty-files --quiet headers.txt '/----------------------------------------------------------/1' '{*}' # split file
curl dict://dict.org/d:word # language dictionary
curl --get --header 'Accept: text/plain' --url https://dontbeevil.rip/search --data-urlencode 'q=QUERY' # devel web api
curl --head https://example.org/ # response header
curl https://icanhazip.com # public ip address
curl --location --write-out "time_namelookup: %{time_namelookup}\ntime_connect: %{time_connect}\ntime_appconnect: %{time_appconnect}\ntime_pretransfer: %{time_pretransfer}\ntime_redirect: %{time_redirect}\ntime_starttransfer: %{time_starttransfer}\ntime_total: %{time_total}\n" https://example.com/
curl --request POST --header 'Content-Type: application/json' --data '{"foo": 1}' https://example.org/ # post web api
curl --verbose https://detectportal.firefox.com # wifi network
cut --delimiter=':' --fields=1 /etc/group | sort
darktable & # 2d image editor raw
date --date="2001-09-09 03:46:40+02:00" +%s # convert timestamp
date --date="6 months ago" # past time
date --date="6 months" # future time
date --date="Friday" # today future midnight
date --date="Monday" # today future midnight
date --date="now" # time
date --date="Saturday" # today future midnight
date --date="Sunday" # today future midnight
date --date="Thursday" # today future midnight
date --date="Tuesday" # today future midnight
date --date="Wednesday" # today future midnight
date --date="yesterday" # time
date +%FT%T.%N # iso time
date --rfc-3339=ns --date="2001-02-03T04:05:06.7 + 1 year 2 months 3 days 4 hours 5 minutes 6.7 seconds" # dst time iso
date --rfc-3339=seconds --date="@1000000000" # convert timestamp
date +%Y-%m-%dT%H:%M:%S # iso time
dconf dump / | dconf2nix # list gnome config
dd if=/dev/cdrom of=cdrom.iso # rip cd
dd if=/dev/null of=/file/to/truncate seek=1 bs=1024 # truncate file bytes
dd if=/dev/urandom bs=1kB count=1 | ent # calculate entropy
dd if=/dev/zero of="${sandbox}/zeros.bin" bs=1000 count=5 # create file size
deactivate # virtualenv
declare -a # arrays
declare -f function_name # function definition
declare -F # functions
declare -p # color variables functions
declare # variables functions
deploy --hostname 127.0.0.1 .#$(hostname) # local nixos
deploy . # nixos
devenv test # nix
devenv update # nix
df --human-readable . # filesystem current directory
df --human-readable # filesystem list all
df --portability . | tail --lines=1 | cut --delimiter=' ' --fields=1 | rg --fixed-strings --invert-match --line-regexp --regexp='-' # directory partition
diff --brief --from-file=FILE --unified FILE… # compare multiple
diffoscope --text-color=always OLD NEW | less --RAW-CONTROL-CHARS # file directory
diff --unified file{.orig,} # files
diff --unified <(hexdump -C "$(which uname)") <(hexdump -C "$(which arch)")
diff --unified <(sudo sh -c env | sort) <(sudo -i sh -c env | sort) # environment variables root login shell
dig example.org # dns lookup internet network
dig example.org mx # dns email lookup internet network
digikam & # photo manager
direnv allow PATH # cd
direnv reload # force rebuild
dirname -- "$PWD" # parent directory
disown %+ # nohup last job background process
dmesg --color=always | less -r # debug os startup
docker build --build-arg=VAR=VALUE --tag=TAG .
docker images # list
docker info
docker manifest inspect MANIFEST # hash digest
docker network inspect host # config
docker ps --all # list container
docker pull REPO/CONTAINER:TAG
docker run --detach --env=POSTGRES_PASSWORD=PASSWORD --name=pg postgres:VERSION && sleep 1s && docker exec --interactive --tty pg psql --user=postgres
docker run --interactive --rm --tty --volume="${HOME}/my projects/root:/etc/nixos" nixos/nix
docker system prune --all --volumes # clean remove container image
dos2unix FILE # convert newline
dot -O -Tsvg ./*.dot # graphics
dot -Tsvg graph.dot # graphics
dotty graph.dot # graphics
dot -V
drive=/dev/sdX && [[ -b "$drive" ]] && ! rg "^${drive}" /proc/mounts && sudo dd bs=4M if=ISO of="$drive" status=progress oflag=sync # iso write disk
dropdb --username username dbname # postgresql
du --si --summarize . # disk size
dvdbackup --mirror --input=/dev/cdrom --output="$HOME" # rip dvd
echo $$ # shell pid
echo $BASHPID # shell subshell pid
echo "${COLUMNS}x${LINES}" # terminal size dimensions
echo $? # exit code
echo "$OSTYPE"
echo "${paths[0]}" # array
echo "${paths[@]: -1}" # array
echo "${paths[@]}" # array
echo "$PATH" | tr ':' $'\n' # user path
echo "${PIPESTATUS[@]}" | tr --squeeze-repeats ' ' + | bc # array sum
echo "$PROMPT_COMMAND" # shell
echo "$REPLY" # read
echo "$TERM" # shell
echo "$WINEPREFIX"
echo 0 61 62 63 | xxd -revert # hex dump convert string character byte
echo "body" > ~/.events/summary # notify
echo 'LC_PAPER="en_GB.UTF-8"' | sudo tee --append /etc/environment # print
echo !!:q | xclip -selection clipboard # copy previous command
eject # hardware cd dvd
enable -a # builtins
enable -n # disabled builtins
env --ignore-environment bash -c 'printf "%s\n" "${?+?=$?}" "${#+#=$#}" "${*+*=$*}" "${@+@=$@}" "${-+-=$-}" "${!+!=$!}" "${_+_=$_}" "${$+$=$$}"; env'
env --ignore-environment HOME="$HOME" USER="$USER" bash --noprofile --norc -o xtrace # test trace shell
env # variable
eval "$(resize -s 24 80)" # terminal
eval "$(ssh-agent)" && ssh-add
eval "$traps" # signal
exec "$SHELL" # replace
exiftool -AllDates+='-00:01:00 00:00:00' . # exif adjust date time images
exiftool -common -json . # exif images metadata directory files
exit
exiv2 print IMG_1234.exv # metadata
expand --tabs=4 FILE # convert tab space
export DISPLAY=:0.0 # remote x display
export key=~/.ssh/id_rsa_service_name && umask 0077 && ssh-keygen -b 4096 -f "$key" -t rsa && openssl pkcs8 -topk8 -v2 des3 -in "$key" -out "${key}.pk8" && shred --remove "$key" && mv --no-clobber "${key}.pk8" "$key" && unset key # create encrypted private public rsa pkcs8 ssh key pair
fab2 --list # fabric python command
fastboot devices # list android
fc-cache # refresh fonts
fc # edit command history
fceux ROM # nes nintendo emulator game
fc-list | sort # list fonts
fc -l # list commands history
fc -s # execute last command history
ffmpeg -activation_bytes ACTIVATION_BYTES -i input.aax -vn -c:a copy output.mp4 # aax audio convert
ffmpeg -i %04d.jpg -vcodec libx264 -bf 0 -crf 12 -vpre medium -an -r 25 -s hd1080 timelapse.mp4 # video convert
ffmpeg -i %04d.jpg -vcodec libx264 -bf 0 -crf 12 -vpre medium -an -r 25 -s hd1080 -vf "transpose=2" timelapse.mp4 # video convert rotate
ffmpeg -i input.mov -vcodec copy -acodec copy -ss 00:00:00 -t 00:01:00 output.mov # video split
ffprobe FILE # media music video metadata
fg # foreground job
file --mime-type PATH
file PATH # type
find . -empty
find . -empty -delete # remove files
find . -exec printf '%s\0' {} + | while read -r -d ''; do printf %q "$REPLY"; printf '\n'; done
find . -group 1000 -exec chgrp "$(id --group)" {} + # update files permissions
find -L . -type l # broken symlinks
find . -mindepth 1 -exec printf '%s\0' {} + | shuf --head-count 10 --zero-terminated # random shuffle files
find . -mindepth 1 -exec printf x \; | wc -c # count files posix safe
find . -path "$(git rev-parse --git-dir)" -prune -o \( -type f -exec rg --files-with-matches $'\t' {} + \) # exclude vcs directories tab files
find /proc -regex '/proc/[0-9].*' -prune -o -print # not process number
find . -regex '.*\(\.orig$\|\(_\(BACKUP\|BASE\|LOCAL\|REMOTE\)_\).*\)' -delete # remove git rebase temp files
find . -type f -executable # files
find . -type f -name '*.odg' -execdir libreoffice --headless --convert-to fodg {} + # convert binary xml
find . -type f -name '*.ods' -execdir libreoffice --headless --convert-to fods {} + # convert binary xml
find . -type f -name '*.odt' -execdir libreoffice --headless --convert-to fodt {} + # convert binary xml
find . -type f -name '*.*' | sed -e 's/.*\.//' | sort | uniq --count | sort --general-numeric-sort # file extensions count
find -version
firefox -no-remote -P PROFILE &
firefox -profilemanager & # config
firefox -safe-mode &
flake8 . # lint python file
fold --width 1 <<< foo # split characters lines
for path in ./*.jpg; do mv --verbose "$path" "$(printf "%04d" "$index").jpg"; ((index+=1)); done; unset index
for path in ./*.mp4; do ffmpeg -ss 1 -i "$path" -qscale:v 4 -frames:v 1 -huffman optimal "${path%.mp4}.jpg"; done # convert video extract frame image podcast
for path in /nix/var/nix/profiles/per-user/$USER/profile-*; do nix-store -q --requisites "$path" | xargs du --si --total | tail --lines=1 | sed "s#total#${path}#"; done # file size derivation
for path in /sys/class/net/*/address; do printf '%s\t%s\n' "$(cut --delimiter=/ --fields=5 <<< "$path")" "$(cat "$path")" ; done # list network interface mac address
for path in ./*.zip; do unzip "$file"; done # all
free --human # memory
full_name="$(getent passwd "$USER" | cut -d ':' -f 5)" && gphoto2 --set-config /main/settings/artist="$full_name" --set-config /main/settings/copyright="Copyright $full_name" --set-config /main/settings/ownername="$full_name" # set camera config
fuseiso -p file.bin "/run/media/${USER}/mountpoint" # mount
fusermount -uz "/run/media/${USER}/mountpoint" # lazy unmount
gcc -Wall -o a.out foo.c # compile
gdalinfo -hist -json -stats FILE # gis image metadata
gdb program # debug
getconf ARG_MAX # arguments
getconf -a # system configuration
getent ahosts example.org # dns hosts internet ipv4 decimal
getent aliases "$USER"
getent hosts example.org # dns hosts internet ipv6 hex
getent passwd "$USER" | cut -d ':' -f 5 # user full name
gethostip -d example.org # dns hosts internet ipv4 decimal
gio mount sftp://HOST # network file ssh /run/user/UID/gvfs/sftp\:host\=HOST/
git add .
git add --patch
git apply - < <(git diff START END)
git bisect reset
git bisect run ./bisect.sh
git bisect skip # broken build
git bisect start BAD GOOD
git blame 1234abcd filename # revision
git blame filename
git branch --all # list local remote
git branch --delete topic # local
git branch --force "$(git default-branch)" "$(git default-remote)/$(git default-branch)"
git branch --format='%(if:notequals=main)%(refname:short)%(then)%(if:notequals=master)%(refname:short)%(then)%(refname:short)%(end)%(end)' --merged | xargs --max-args=1 --no-run-if-empty git branch --delete
git branch --list --remote 'REMOTE/*'
git branch --remotes
git branch --set-upstream-to=UPSTREAM/BRANCH BRANCH # track
git branch --track BRANCH START
git -C "$(git rev-parse --show-toplevel)" checkout-index --all --prefix="../$(basename "$(git rev-parse --show-toplevel)")-copy/" # copy repository file
git checkout -b BRANCH "$(git default-remote)/$(git default-branch)" # branch change create
git checkout BRANCH # branch change
 git checkout includes/.bash_history
git checkout . # revert unstaged change
git cherry-pick COMMIT # apply
git clean --exclude=/.idea -dnx # directories .gitignore
git clone git@gitlab.com:engmark/root.git
git co-author
git co-author 'NAME <EMAIL>'
git commit --all --message='chore: Delete me' # temp
git commit --amend
git commit --edit --file="$(git rev-parse --git-dir)/COMMIT_EDITMSG" # resume
git commit --fixup=ID # prepare autosquash omit message
git commit --squash=ID # prepare autosquash include message
git config branch.master.rebase true
git config diff.minimal
git config diff.minimal false
git config diff.minimal invalid
git config diff.minimal true
git config --global mergetool.prompt false
git config --list
GIT_CURL_VERBOSE=1 git pull # debug
git diff
git diff --cached # stage
git diff --color-words
git diff | diff-ignore-moved-lines
git diff --exit-code # check
git diff FROM..TO
git diff 'HEAD@{2013.04.30 08:00}' # date
git diff HEAD^ -- PATH # last commit
git diff --ignore-all-space
git diff --ignore-all-space --no-color | git apply --cached
git diff --ignore-space-change
git diff --raw
git diff --staged
git diff --staged --ignore-space-at-eol
git diff --staged --stat
git difftool old new # gui
git diff --word-diff
git fetch --all
git fetch && git merge FETCH_HEAD # pull
git fork UPSTREAM DOWNSTREAM # clone
git format-patch --find-renames --in-reply-to=MESSAGE_ID --signoff --stdout HEAD^ | git imap-send # email
git gc # garbage collect
git --git-dir=../other/.git format-patch --keep-subject -1 --stdout HEAD | git am --keep --3way # cherry-pick commit head repo
git grep -I --name-only --null -e '' | xargs --null sed --in-place 's/[ \t]\+\(\r\?\)$/\1/;$a\' -- # git whitespace eol eof
git gui &
git help SUBCOMMAND
git init
git instaweb start
git instaweb stop
gitk --all & # gui branch
git log
git log --after '2012-01-20 17:06' --before '2012-04-03 16:15' # date range
git log --all --decorate --graph # branch
git log -L :FUNCTION:FILE # follow
git log --oneline --decorate
git log --patch-with-stat
git log --stat
git ls-files 'GLOB' # list files
git ls-files ':!:*.*' # list files missing dot
git ls-tree --long HEAD ./* | awk '{print $4 " " $5}' # file size commit
git merge --abort # cancel revert
git merge origin/master
git mergetool
git merge topic # local branch
git most-edited . # count file
git mv README{,.markdown}
git pull --all
git pull --rebase --autostash
git pull --recurse-submodules=yes --update-head-ok origin master # follow initial fetch
git push --atomic origin "$(git branch --show-current)" TAG
git push --delete origin ID # delete remote branch tag
git push --force-with-lease
git rebase --abort
git rebase --continue
git rebase --interactive "$(git default-remote)/$(git default-branch)" # change local commits
git rebase --onto="$(git default-remote)/$(git default-branch)" BRANCH # change other branch
git reflog
git reflog show --patch-with-stat
git remote add -f origin ssh://user@example.org/home/user/repo
git remote --verbose # list
git reset --soft HEAD~1 # commit undo
git reset v0.1
git restore --staged PATH # unstage
git revert COMMIT # commit undo
git revert --no-commit OLD..NEW # undo multiple
git shortlog --all --email --numbered --summary # author list
git show COMMIT:FILE # contents
git show --format=%B --no-patch ID # print message
git stash drop stash@{INDEX} # delete
git stash && git pull && git stash pop
git stash && git rebase --interactive HEAD~20
git stash --keep-index --include-untracked # stage
git stash list --date=local
git stash list --patch # diff
git stash --patch # diff create
git stash pop stash@{INDEX}
git stash save --keep-index "message"
git stash save "message"
git stash save --patch "message" # diff create
git stash show
git stash show --patch # diff
git stash show --patch stash@{INDEX} # diff
git status
git submodule add --branch BRANCH --name NAME URL
git submodule update --init
git subtree add --prefix=example REPO BRANCH # merge repository history
git tag --delete name # local
git tag --sort=version:refname # order list
git tag v0.1
glab auth login # authenticate gitlab
glabels & # graphics
glab repo clone --archived=false --group=GROUP --paginate # git gitlab
glc-capture --out recording.glc minecraft
glc-play recording.glc
glc-play recording.glc -a 1 -o recording.wav # audio
glc-play recording.glc -y 1 -o - | mencoder -demuxer y4m - -ovc lavc -lavcopts vcodec=mpeg4:vbitrate=3000 -audiofile recording.wav -oac mp3lame -o recording.avi # video encoding mp4 mpeg4
glxgears # 3d graphics benchmark
glxinfo
gnome-extensions list
gpg --allow-secret-key-import --import ~/secring.gpg
gpg --armor --export ID # print public key text
gpg --decrypt encrypted.asc
gpg --decrypt | gpg --import # import key signature
gpg --delete-keys ID
gpg --edit-key ID
gpg --export --armor --output public.asc ID # key file
gpg --export-secret-keys --armor ID --output private.asc # key file
gpg --full-gen-key # create key
gpg --gen-revoke --output ID.rev ID # generate revoke key certificate file
gpg --import ~/pubring.gpg
gpg --keyserver keys.gnupg.net --receive-keys 55D0C732 # import pgp signature
gpg --list-public-keys --with-fingerprint # print public keys
gpg --list-secret-keys --with-fingerprint # print private keys
gpg --list-sigs 92126B54 # key signature
gpg --search EMAIL # public key
gpg --send-key 92126B54 # upload
gpg --verify ./*.sig # pgp signature
gphoto2 --abilities # camera
gphoto2 --auto-detect # list camera
gphoto2 --capture-movie # video
gphoto2 --list-all-config # camera
gphoto2 --stdout autofocusdrive=1 --capture-movie | ffmpeg -i - -vcodec rawvideo -pix_fmt yuv420p -threads 0 -f v4l2 /dev/video0
gphoto2 --summary # list camera
groups
groups "$USER"
gsettings list-recursively # gnome config
gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=unencrypted.pdf -c .setpdfwrite -f encrypted.pdf # pdf remove password
gunzip FILE
gzip FILE # compress
hash # list program recent
help # list all bash builtin
history | less
host example.org # dns lookup internet network ipv4 ipv6
hostid # host id hex
hostname # internet network
hostname --ip-address # internet network address dns
hostname --short # internet network
hugin & # panorama 2d photo stitch
hyperfine COMMAND # timing performance
iconv --from-code=utf-8 --to-code=iso-8859-1 utf8.txt > latin1.txt # convert encoding
id "$USER"
idea-community diff LEFT RIGHT BASE &
id --group
id # group user
id --user
if [[ -r /proc/sys/kernel/ns_last_pid ]]; then while true; do while read -r; do if [[ "$REPLY" != "$old" ]]; then printf '%(%s)T %d\n' -1 "$REPLY"; old="$REPLY"; fi; done < /proc/sys/kernel/ns_last_pid; read -r -t 1 || true; done; fi # processes pids log
IFS=':' read -a paths -r <<< "$PATH" # tokenize array
img2scad < test.png # openscad convert image
indent ./*.c
indentect < "$(which indentect)"
indentect --verbose < "$(which indentect)"
infocmp -1 # list terminal
info --raw-escapes --subnodes COMMAND | less --raw-control-chars # help gnu
inkscape & # editor svg
inxi --full # list hardware cpu graphics bluetooth audio network disk
iostat
ip addr show dev eth0 # ipv4 ipv6 device address
ip addr show # ipv4 ipv6 network address
ipcalc 0.0.0.0/0 # network
ipcalc 192.168.0.1/24 # network
ip link show down # network
ip link show up # network
ip -oneline -family inet addr show dev eth0 # ipv4 address
ip route show dev eth0 # network
ipython
iwconfig # wireless network configuration
iwlist scanning
java -jar ~/schemaSpy.jar -dp /usr/share/java/mysql.jar -hq -t mysql -host localhost:3306 -db database -u user -p password -o ~/db && x-www-browser file://"$HOME"/db/index.html # mysql db visualization
java -jar ~/schemaSpy.jar -dp /usr/share/java/postgresql.jar -hq -t pgsql -host localhost:5432 -db database -s public -u user -p password -o ~/db && x-www-browser file://"$HOME"/db/index.html # postgresql db visualization
java -Xmx4096M -Xms512M -jar ~/.minecraft/launcher.jar
jhead -da2010:11:12/13:14:15-2005:01:01 FILE # adjust date time image
jobs -l # list all background
journalctl --boot=-1 --catalog --output=short-iso # previous
journalctl --boot --catalog --output=cat --unit SERVICE # message
journalctl --boot --catalog --user-unit SERVICE
journalctl --catalog --follow
journalctl --catalog --lines=10
journalctl --catalog --since=today --output=short-iso SYSLOG_IDENTIFIER="xprofile-${USER}" # log user x profile
journalctl --catalog --since=today --output=short-iso # systemd service log
journalctl --catalog --since=today --unit=shadow.service
journalctl _SYSTEMD_INVOCATION_ID="$(systemctl show --property=InvocationID --value SERVICE)" # service log since start
jq . <<< '{"Hello": "World"}' # json pretty-print input
jq . < *.json # json pretty-print file
jq --null-input --arg name 'Jane Doe' --arg password '$ec\ret' '{"name": $name, "password": $password}'
jq 'PATH = VALUE' FILE | sponge FILE # modify
jq 'select(.KEY == VALUE) // error("MESSAGE")' FILE # assert
jupyter kernelspec list
jupyter lab PATH # open notebook ipython
jupyter nbconvert --clear-output --ClearMetadataPreprocessor.enabled=True FILE # ipynb clean
jupyter nbconvert --to=script FILE # ipynb export python
jupyter notebook # create file
kill -0 "$!" # check last job background process pid
kill -18 "$!" # resume last job background process pid
kill -20 "$!" # suspend last job background process pid
killall process
kill -INT $$ # signal
kill -l # list signals
ktrash --empty # kde trash empty delete
kubectl api-resources --sort-by=name # list
kubectl apply --dry-run=client --filename=FILE
kubectl get namespaces # list
kubectl get nodes --output=wide
kubectl --namespace=NAMESPACE config set-context --current
kubectl --namespace=NAMESPACE delete pods POD…
kubectl --namespace=NAMESPACE describe pod POD
kubectl --namespace=NAMESPACE exec --stdin --tty POD -- bash # debug interactive shell container
kubectl --namespace=NAMESPACE get deployments
kubectl --namespace=NAMESPACE get --field-selector=type=Warning --sort-by=.metadata.creationTimestamp events
kubectl --namespace=NAMESPACE get pods
kubectl --namespace=NAMESPACE get services
kubectl --namespace=NAMESPACE logs --follow POD
kubectl --namespace=NAMESPACE port-forward service/SERVICE PORT
kubectl --namespace=NAMESPACE rollout restart deployment DEPLOYMENT
kvm -boot d -m 512 -cdrom *.iso # vm
kvm -hda /tmp/my.img -cdrom *.iso -boot d # vm
last | less # list all user login history
lastlog # login users
LC_COLLATE=en_NZ.utf-8 sort --output=.gitignore --unique .gitignore
ldconfig --print-cache # list all libraries
ldd "$(which bash)"
less --RAW-CONTROL-CHARS FILE # color
less "/usr/share/X11/locale/$(rg --max-count=1 "${LANG%.*}.UTF-8\$" /usr/share/X11/locale/locale.dir | cut --delimiter=/ --fields=1)/Compose" # keyboard shortcuts
libreoffice &
ln --symbolic -- target source
locale
locale --all-locales
localectl list-keymaps # keyboard layout list
localectl list-x11-keymap-layouts # keyboard layout list
localectl list-x11-keymap-models # keyboard list
localectl list-x11-keymap-variants us # keyboard list
locate FILE
loginctl --output=json-pretty list-sessions # user
loginctl --output=json-pretty show-session "$(loginctl --output=json-pretty list-sessions | jq --raw-output '. | map(.session) | join(" ")')" # user
lorri watch --once # nix shell compile wait
lpinfo -m # printer model driver list
lpoptions -l # printer list
lpstat -d # default printer cups
lpstat -v # list all printers cups
lsblk # list block device disk
lsblk --noheadings --output UUID /dev/mapper/vg-swap # disk uuid
lsb_release --all # linux version distro
lscpu # hardware architecture processor
lscpu | rg '^CPU op-mode' # detect cpu bit mode
ls --directory /proc/[^0-9]* # metadata
lshw
ls -l --all # list
ls -l --block-size 1 # list files size bytes
ls -l --directory ./*/ # list directories
lslocks # list file locks
ls -l --reverse # list
ls -lt /var/log/ # sort time list
lsmod # kernel modules
lsof +c 0 | rg process-name | wc --lines # count files
lsof +D /path # list open file directory recursive
lsof -i :22 # internet port network
lsof -i tcp # internet network
lsof +L1 # list deleted files
lsof # list open files
lsof -p $$ # files process
lspci # list pci hardware
lspci | rg --ignore-case audio # pci device
lspci -v -s 00:1b.0 # pci device details
lsusb # list usb hardware
lsusb | rg --ignore-case cam
makepkg --printsrcinfo > .SRCINFO # package generate metadata
makepkg --syncdeps --force # package build rebuild
makepkg --syncdeps --install # package build tarball install
man 1p COMMAND # posix help
man 5 FORMAT # config file format help
man --global-apropos --where REGEX # full search
mapfile -d '' array < <(COMMAND) # nul separated array
mcomix &
meld . & # diff
meld original mine & # diff
meld other original mine & # diff
meld <(ssh example.org cat /etc/hosts) <(ssh example2.org cat /etc/hosts) # diff
meld <(wget --output-document - http://git.gnome.org/browse/meld/plain/.gitignore?id=250066249e06241e3bfd3863c1a233fb45f40a12) <(wget --output-document - http://git.gnome.org/browse/meld/plain/.gitignore) # diff
mencoder -fps 10 -nosound -ovc copy timelapse.mp4 -o timelapse-slow.mp4 # video
mkfifo pipe1 pipe2
modinfo i915 # kernel module
mogrify -crop 2316x1303+0+0 FILE
mogrify -resize '1920x1080>' FILE # resize 1080p height
mount # list all
mount --no-mtab --options remount,defaults /dev/sda1 /
mountpoint /home
mp3fs -obitrate=256 ~/music/ ~/mp3 # mount
mpv --ytdl-raw-options=cookies-from-browser=firefox URL # yt-dlp
mtr HOST # trace ping check reliability
mutmut results # mutation test summary
mutmut show all # mutation test list diff
mv file{.orig,}
~/my\ projects/img2vcard/img2vcard.sh photo.jpg > photo.vcf # convert image vcard
~/my\ projects/meta2jpeg/meta2jpeg.sh ./*.CR2 # copy metadata graphics image
~/my\ projects/schemaspy2svg/schemaspy2svg.sh ~/db # database graphics convert svg
~/my\ projects/xterm-color-count/xterm-color-count.sh -v # xterm color
~/my\ projects/xterm-color-count/xterm-color-count.sh # xterm color
mypy --command CODE # inline check python
mypy --install-types # suggest package python
mypy --strict . # python check type typing file
ncal -3bM
ncdu --extended --one-file-system PATH # find large directory file
nc -l 12345 & ss --all --tcp | rg :12345 && kill "$!" # test network listen port
neato -O -Tsvg ./*.dot
newgrp GROUP # change default group
nice --adjustment 19 ionice --class 3 du | sort --numeric-sort --key 1 # priority cpu io
nix-build --attr inputDerivation shell.nix | cachix push CACHE
nix-build --attr pkgs.PACKAGE --keep-failed --show-trace FILE
nix-build nixos --include nixos-config=config.nix --attr 'config.environment.etc."PATH".source' # build nixpkgs file contents
nix develop # flake
nix develop --profile dev-profile --command true && cachix push CACHE dev-profile # cache flake
nix-du --root "$(nix derivation show .#devShells."$(nix eval --expr builtins.currentSystem --impure --raw)".default | jq --raw-output 'keys[]')" | dot -Tsvg > nix-du.svg # derivation disk space graph
nix-env --list-generations
nix-env --query --attr nixos.PACKAGE --available --json --meta # package property
nix-env --query # list environment package
nix eval --expr 'let pkgs = import <nixpkgs> {}; in builtins.toString (builtins.map (maintainer: "@${maintainer.github}") pkgs.PACKAGE.meta.maintainers)' --impure --raw # package username
nix eval --file '<nixpkgs>' --raw PACKAGE # package path
nix flake archive --json | jq --raw-output '.path,(.inputs|to_entries[].value.path)' | cachix push CACHE # cache
nix flake check
nix flake init --template github:nix-community/poetry2nix # create project
nix flake update
nix fmt
nixfmt FILE # format nix
nix-info --markdown | sed "s/$USER/username/" # package metadata
nix-instantiate shell.nix # derivation path
nix-locate PATH # search store
nix optimise-store # clean storage
nixos-option environment.systemPackages # doc help
nixos-rebuild repl # inspect system config
nixos-rebuild switch --flake .#HOSTNAME --target-host HOST --use-remote-sudo
nixos-version
nixos-version --hash
nix path-info --human-readable --closure-size "$(nix derivation show .#devShells."$(nix eval --expr builtins.currentSystem --impure --raw)".default | jq --raw-output 'keys[]')"
nixpkgs-review pr --post-result ID… # build test comment github
nixpkgs-review rev HEAD # build test local
nix-prefetch-url --unpack URL # checksum
nix repl
nix repl ".#nixosConfigurations.\"$(hostname)\""
nix search --update-cache STRING # package
nix-shell -I nixpkgs=https://github.com/NixOS/nixpkgs/archive/ID.tar.gz --packages PACKAGE # install unstable
nix-shell --keep VARIABLE --keep VARIABLE --packages PACKAGE… --pure --run 'COMMAND ARGS…' # install
nix-shell maintainers/scripts/update.nix --argstr package PACKAGE # nixpkgs upgrade
nix-shell --packages deadnix --pure --run 'deadnix --edit .'
nix-shell --packages ghostscript_headless --run 'ps2pdf IN OUT' # convert postscript
nix-shell --packages 'python3.withPackages(ps: [ps.jupyter-client ps.nix-kernel])' # interpreter
nix-shell --packages statix --pure --run 'statix fix .'
nix-shell --pure --run 'pre-commit run --all-files'
nix show-config # list
nix show-derivation --file NIX_FILE # pretty format
nix show-derivation /nix/store/*-PACKAGE.drv # pretty format package
nix-store --query --graph PATH | dot -Tsvg > dependencies.svg # reference package
nix-store --query --references "$(nix-instantiate shell.nix)" # list dependency
nix-store --query --references /run/current-system/sw | cut --delimiter=- --fields=2 | sort --unique # list installed package
nix-store --query --referrers PATH… # closure reverse dependency
nix-store --query --requisites /nix/var/nix/profiles/system | cut --delimiter=- --fields=2 | sort --unique # list installed package
nix-store --query --requisites PATH… # store path closure dependency
nix-store --query --tree PATH… # store path closure recursive dependency
nix-tree PATH # store path closure recursive dependency
nix why-depends ANCESTOR DESCENDANT # filter dependency tree
nl FILE # number lines
nmap -p 22 --open TARGET # search list ssh hosts network
nmap -T Aggressive -A -v TARGET # os host
nmap -v -sP TARGET # list ip hosts network
nmcli connection show --active
nm libfoo.so | rg '^ *U ' # dev undefined object
node # javascript
node -v
nohup idea-community nosplash . &>/dev/null & disown # project background
notify-send "summary" "body"
npm install --local # package.json node.js
npm list PACKAGE # tree reverse dependency
npm run RUN_OPTIONS COMMAND -- COMMAND_OPTIONS
nproc # print processor count
nslookup example.org # dns internet lookup network
ntpq --peers # query list peers offset time
nvtop # monitor gpu
openscad ~/my\ projects/crumbling-beaker/beaker.scad &
openssl asn1parse -in ~/.ssh/id_?sa # decode key
openssl genrsa -des3 -out private.pem 4096 # create des3 encrypted private rsa key hex
openssl pkcs12 -export -inkey name.key.pem -in name.crt.pem -out name.p12 # merge certificate key
openssl pkcs12 -info -in name.p12 -noout # list verify
openssl req -new -key private.pem -out request.pem # create x509 certificate signing request hex
openssl req -text -in name.csr.pem # list certificate request properties
openssl rsa -text -in name.key.pem # list key properties
openssl s_client -connect example.com:443 -status <<< '' # get certificate connect host ssl https
openssl x509 -req -days 1 -in request.pem -signkey private.pem -out certificate.pem # create self-signed x509 certificate hex
openssl x509 -text -in name.crt.pem -noout # list x509 certificate properties
oxipng --opt=max *.png # compress
pacmd list-sinks # audio output
pandoc --output=./README.html --standalone --metadata=pagetitle=TITLE --to=html ./README.md # convert markdown markup
parallel git -C {} checkout-index --all --prefix="$PWD"/'"$(basename "$(dirname "$(dirname {})")")"/' ::: ~/*/.git/.. # backup repos
parallel "git -C {} diff --quiet || git -C {} rev-parse --show-toplevel" ::: ~/*/.git/.. ~/my\ projects/*/.git/..
parallel "git -C {} rev-parse --show-toplevel && git -C {} pull --all --tags" ::: ~/*/.git/.. ~/my\ projects/*/.git/..
parallel "git -C {} stash list --exit-code >/dev/null || git -C {} rev-parse --show-toplevel" ::: ~/*/.git/.. ~/my\ projects/*/.git/..
parse-edid < ./edid # monitor resolution
passwd # password user
patch --strip 0 < patch.diff
PATH=$(IFS=':'; echo "${paths[*]}")
pathchk --portability PATH # check path posix
pavucontrol & # pulseaudio volume
pdfimages -j ./*.pdf .
pdfinfo file.pdf
perl -MFile::stat -MData::Dumper -le '$f = stat shift; print Dumper($f);' PATH # print file metadata
perl -ne 'print join("\n", split(/:/));print("\n");' input # split join
perl -pe 'chomp if eof' input > output # remove newline eof
pgrep -P $$ # child processes pids
pgrep -u root cron
pidof COMMAND # process pid
pijul record # commit
ping -c 4 example.org
pip-compile --allow-unsafe --generate-hashes --no-annotate --no-header --output-file=requirements.txt <(echo PACKAGE)
pip freeze > requirements.txt # save python dependencies
pip install --requirement requirements.txt
pip install --upgrade pip
pngcrush -brute -d target ./*.png # graphics compress
pod2text file.pl # perl documentation
poetry add --allow-prereleases --extras='FIRST SECOND' --group=dev --lock --optional -vv black="*" # install python package
poetry cache clear --all . # python
poetry config --list # python
poetry config --local virtualenvs.in-project true # python
poetry init --name=PROJECT --author='NAME <USER@HOST>' --python=VERSION --dependency='PACKAGE=*' --license=LICENSE --no-interaction # python package
poetry install --all-extras --no-root --sync # python package
poetry lock --no-update -vv # python
poetry show --outdated # python package update
poetry update --lock -vv # python package
pre-commit autoupdate --freeze
pre-commit install --hook-type=commit-msg --hook-type=pre-commit --overwrite
pre-commit run --all-files
printf $"\0" | uni identify # 1 nul escape quote
printf $'\0' | wc --chars # 0 escape quote
printf "${USER}%.0s" {1..5} # repeat string
printf 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ | base64 --wrap=0 # encode
printf "\0" | uni identify # 1 nul escape quote
printf '\0' | uni identify # 1 nul escape quote
printf \\0 | uni identify # 1 nul escape quote
printf \0 | uni identify # 1 zero escape quote
printf MDEyMzQ1Njc4OWFiY2RlZmdoaWprbG1ub3BxcnN0dXZ3eHl6QUJDREVGR0hJSktMTU5PUFFSU1RVVldYWVo= | base64 --decode
printf '%q\n' "${array[@]}"
printf '%q\n' "$IFS" # escape bash
printf '%q\n' "$PS1" # escape bash
printf %s $'--$`!*@\a\b\E\f\r\t\v\\\'"\360\240\202\211 \n' | uni identify # test unicode
printf %s $'--$`!*@\a\b\E\f\r\t\v\\\'"\360\240\202\211 \n' | xclip; cmp --verbose <(printf %s $'--$`!*@\a\b\E\f\r\t\v\\\'"\360\240\202\211 \n') <(xclip -out) # clipboard copy test
printf %s "${IFS:0:1}"
printf %s "$IFS" | od --format x1 # string character byte convert hex dump posix
printf %s "$IFS" | xxd -groupsize 1 # string character byte convert hex dump
PS1='\$ ' # prompt demo
ps afux | less -S # processes list all tree tty
ps -eo user= | sort | uniq --count | sort --reverse --numeric-sort # processes users
ps -p $$ o ppid= # parent pid
ps --pid $$ # current shell
ps --pid "$(find -L /proc/[0-9]*/exe ! -type l | cut --delimiter='/' --fields='3' | paste --serial --delimiters=',')" # non-kernel processes
psql --host localhost --port 15432 --dbname postgres --username postgres <<< "COPY(SELECT datname FROM pg_database WHERE datistemplate = FALSE) TO STDOUT;" # forwarding list all postgresql network
ps -U root -u root fu | less -S # processes list user tree
ps uw -p $$ # process single pid
ptouch-print --text 'first line' 'second line' # label
pwd # current directory
pycodestyle --max-line-length=120 # python lint style check file
pyenv install --list # python version
pyenv install # python
pyenv update # python upgrade
pylint --generate-rcfile # config python
pytest --disable-socket --doctest-modules -m 'MARKER and not MARKER' --verbosity=2 # offline filter python
python -m py_compile script.py # verify code script
python -m pydoc # help documentation
python -m timeit -s 'text = "sample string"; char = "g"' 'char in text' # timing benchmark
python -m unittest discover
python -m venv .venv # virtualenv
qemu-img create -f qcow2 /tmp/my.img 10G
qpdf --empty --pages first.pdf second.pdf -- target.pdf # concatenate files
qpdf --pages input.pdf 1-2,4-z -- input.pdf output.pdf # remove page
qpdf --split-pages INPUT page-%d.pdf
qprint --decode # quoted-printable
qprint --encode # quoted-printable
qr2scad < ~/my\ projects/qr2scad/tests/example_big.png > big.scad
rclone config
rclone copy --progress PATH SERVICE: # transfer copy file network
readelf --all "$(which readelf)" # executable binary
readlink /proc/$$/fd/0 # symlink source pipe file descriptor pseudo terminal
read -r <<< "$text"
read -r < "/path"
read -r var
reset # clear log remove terminal text
rg ":$USER\$" /etc/group
rg "^$USER:" /etc/passwd # password
rg '(\b|^)COMMAND\b.* .*help' ~/.bash_history # search
rg --files-with-matches --null --regexp=PATTERN ./* 2>/dev/null | tr --complement --delete '\000' | wc --chars # count occurrences pattern
rg --files-with-matches --null --regexp=PATTERN ./* | xargs -0 rg --files-with-matches --regexp=OTHER # search and patterns
rg --glob=!EXCLUDE PATTERN .
rg 'GREEK SMALL LETTER PI' "/usr/share/X11/locale/$(rg --max-count=1 "${LANG%.*}.UTF-8\$" /usr/share/X11/locale/locale.dir | cut --delimiter=/ --fields=1)/Compose" # unicode character
rg --no-filename --only-matching PATTERN . | wc --lines # sum count search
rg --passthru --regexp="^$USER:" /etc/passwd # context highlight
rg PATTERN # search
rmdir ./*
rsync --archive --human-readable --progress --verbose --remove-source-files source host:target # network transfer move
rsync --rsh='ssh -p 2020 -i ~/.ssh/host.pem' --archive --human-readable --progress --verbose host:/path /target # network transfer
runlevel # kernel
rustup default stable # rust install toolchain
rustup update # rust package update
sane-find-scanner # list device
scanimage --list-devices # scanner
scc . # sloc cloc code count
script --quiet --command "$SHELL"
scrot --delay 5 # screenshot
scrot --select # screenshot area selection
sed '/^$/d' file # delete empty lines
sed '9d' file # delete line one-indexed
sed '/PATTERN/r./FILE' FILE # insert merge file pattern
sed --quiet '/^START$/,/END^$/{/^START$/d;/^END$/d;p;}' <<< $'START\nfirst\nEND\nSTART\nsecond\nEND' # extract delimiter lines
sed '/^[[:space:]]*$/d' file # delete whitespace line
sensors # temperature cpu gpu
set +o noclobber # file error
set -o noclobber # file error
set +o nounset # variable error
set -o nounset # variable error
set +o pipefail # error
set -o pipefail # error
set +o xtrace # disable
set -o xtrace # enable
setxkbmap -device "$(which-keyboard)" -layout us -variant dvorak-alt-intl -option compose:caps # keyboard layout config
setxkbmap -layout us # keyboard layout qwerty us reset
setxkbmap -print # keyboard settings
setxkbmap -verbose | awk -F '+' '/^symbols:/ {print $2}' # keyboard layout settings
sh
sha256sum --check ./*.sha256 # verify checksum
sha256sum file # print checksum
sha512sum --check ./*.sha512 # verify checksum
sha512sum file # print checksum
shellcheck --enable=all --severity=style FILE # strict
shellcheck --format=json FILES | jq --raw-output '.[].file' # list
shfmt --write FILE # format shell script
shopt nullglob # option get
shopt # options
shopt -s dotglob # option set .*
shopt -s extglob # option set ?(a|b) *(a|b) +(a|b) @(a|b) !(a|b)
shopt -s globstar # option set **
shopt -s globstar && wc --lines ./**/**.ext # count recursive lines
shopt -s nullglob # option set *
shopt -u nullglob # option unset *
showkey # keyboard console
shred --remove PATH # overwrite delete file
shuf --input-range 0-1 --head-count 1 # random number range
simple-scan &
sipcalc 0.0.0.0/0
sipcalc 192.168.0.1/24
sleep 1m
sops edit FILE # encryption
sops updatekeys FILE # sync encryption
speaker-test --channels 2
speaker-test --device plughw:2,3 # test audio device
sqlite3 -line ./*.sqlite3 "select * from TABLE"
ss --all # internet connections sockets
ssh-add KEY
ssh-add -l # list key fingerprint
ssh-add -L # list key parameter
ssh -A example.org # forward agent key
ssh-copy-id example.org
ssh -i ~/.ssh/server.pem user@example.org # alternative key
ssh-keygen -f KEY -e -m PKCS8 | openssl pkey -pubin -outform DER | openssl md5 -c # key fingerprint aws emulate ec2-fingerprint-key
ssh-keygen -f ~"/.ssh/known_hosts" -R '[1.2.3.4]:1234' # remove public key
ssh-keygen -l -f /etc/ssh/ssh_host_ecdsa_key.pub # host id ecdsa fingerprint
ssh-keygen -l -f /etc/ssh/ssh_host_rsa_key # host id rsa fingerprint
ssh-keygen -y -f ~/.ssh/id_rsa > ~/.ssh/id_rsa.pub # generate public key
ssh-keyscan HOST # public
ssh-keyscan HOST | ssh-to-age # encryption
ssh -L 15432:localhost:5432 example.org # local port forwarding postgresql
ssh -L 5902:localhost:5901 example.org # local port forwarding vnc
ssh -L 8080:localhost:8081 proxy.example.com -t ssh -D 8081 endpoint.example.com # ssh network proxy
ssh -p 22222 example.org
ssh -R 9000:localhost:9000 example.org
ssh -t localhost sudo -u nobody env # tty
ssh -vvv example.org
ssh -Y example.org # trusted x11 forwarding
ss --tcp --udp --listening --numeric --processes # network service
stat --format '%A %U %G %s %y %n' ./* # list permissions user group file
stat --format %i / # inode
stat --printf '%A %U %G %s %y %n\0' ./* # list permissions user group file nul
strace -Cf bash -lc true # count calls profile summary
strace -fe open FILE 2>&1 >/dev/null | rg --only-matching '^(\[pid\s+[0-9]*\] )?open\("[^"]+' | rg --only-matching '".*' | cut --characters 2- | sort --unique # script dependencies
streamlink http://www.twitch.tv/foo best &
strings "$(which strings)"
stty --all # terminal settings
stty sane # restore terminal state
sudo badblocks -sv /dev/sda # check disk health
sudo blkid -o list
sudo cachix use CACHE
sudo chfn -f "My Name" "$USER" # full name
sudo chgrp --recursive nogroup -- PATH # set group file
sudo chown "$USER":"$(printf %q "$(groups | awk '{print $1}')")" "/run/media/${USER}/mountpoint"
sudo chown nobody "$sandbox"
sudo chroot /var/jail/"$USER" su --login "$USER" # jail
sudo crontab -e # edit
sudo cryptsetup luksAddKey /dev/DISK # luks add key password passphrase disk
sudo cryptsetup luksDump /dev/DISK # list property luks
sudo cryptsetup status lvm # encryption disk
sudo delgroup GROUP # delete group
sudo deluser USER # delete user
sudo dmidecode | less
sudo dmidecode --string system-product-name
sudo dmidecode --string system-serial-number
sudo dmidecode --type 1
sudo dmidecode --type memory | grep Width | paste --delimiters=' \n' --serial # detect ecc ram
sudo dmidecode --type system
sudo /etc/init.d/postgresql reload
sudo extundelete --restore-all /dev/sdx1
sudo faillog # log summary user
sudo fdisk /dev/sdx # partition disk
sudo fdisk -l # list all disks
sudo file /boot/vmlinuz-linux | rg --fixed-strings --word-regexp --quiet "$(uname -r)" || echo 'You should reboot to use the new kernel'
sudo file --special-files /dev/sdX # detect disk type
sudo fsck -t exfat /dev/sdx # check exfat filesystem
sudo fwupdmgr get-devices # list firmware upgrade
sudo fwupdmgr refresh # firmware upgrade
sudo fwupdmgr update # firmware upgrade
sudo gpasswd --add "$USER" group # user add group
sudo groupadd GROUP # add create group
sudo groupdel GROUP # delete remove group
sudo grpck --sort
sudo grub-install /dev/sda # fix boot mbr
sudo hddtemp --unit=C $(lsblk --nodeps --noheadings --output=path) # temperature
sudo hdparm -I /dev/sda # harddisk hardware properties
sudo hwclock # hardware time
sudo iftop # bandwidth network interface monitor
sudo iftop -i wlp1s0 # bandwidth network interface monitor wireless
sudo iotop --batch --iter 1 # i/o storage
sudo iotop # i/o storage repeat
sudo iotop --pid=$$ # i/o process
sudo ip6tables --list-rules # list all ipv6 firewall rules network commands
sudo ip addr add 1.2.3.4/24 dev eth0 # network address
sudo ip addr del 1.2.3.4/24 dev eth0 # network address delete
sudo ip link set eth0 down # network disable device
sudo ip link set eth0 up # network enable device
sudo ip route add default via 1.2.3.4 dev eth0
sudo ip route del default via 1.2.3.4 dev eth0
sudo iptables --list-rules # list all ipv4 firewall rules network commands
sudo kdiff3 --output /etc/something.conf{,,.pacnew} # merge
sudo kismet # wireless network monitor
sudo lastb | less # list all bad failed user login history
sudo ldconfig
sudo lightdm-gtk-greeter-settings # login config
sudo --list # show user capabilities
sudo ln --force --symbolic /usr/share/zoneinfo/Europe/London /etc/localtime # set system timezone
sudo ln --symbolic "/run/media/${USER}/windows/Windows/Fonts" /usr/share/fonts/WindowsFonts # enable windows fonts
sudo loadkeys dvorak # keyboard layout vt terminal
sudo localectl --no-convert set-keymap dvorak # x11 console vt keyboard layout
sudo localectl --no-convert set-x11-keymap us '' dvorak-alt-intl
sudo lpadmin -d printername # set default printer cups
sudo lshw | less
sudo mandb # update apropos database manual
sudo mkdir "/media/mountpoint"
sudo mkfs.ext4 /dev/sdx1 # format ext4 partition
sudo mkfs.msdos -F 32 /dev/sdxy # format fat32 partition
sudo modprobe microcode # add insert kernel module
sudo mount --all
sudo mount --options remount,ro /dev/sda1 # readonly restore
sudo mount --options remount,rw /dev/sda1 # writeable
sudo mount --types tmpfs --options size=1m tmpfs -- "$(mktemp --directory --tmpdir -- ramdisk.XXXXXXXXXX)" # create ramdisk partition
sudo nethogs wlan0 # network monitor
sudo nix-collect-garbage --delete-older-than 7d
sudo nix-env --list-generations --profile /nix/var/nix/profiles/system
sudo nix-env --profile /nix/var/nix/profiles/system --delete-generations +3 # remove cleanup old
sudo openvpn --config /etc/openvpn/client/config.ovpn
sudo os-prober # boot
sudo paperconfig --paper a4 # set print size
sudo passwd --delete root # disable account user
sudo pip install --upgrade pip # python
sudo powertop # power monitoring configuration
sudo puppet apply --modulepath modules manifests/host.pp # configuration
sudo pwck --sort
sudo qemu-img convert -c -O qcow2 /var/lib/libvirt/images/*.qcow2 NAME.qcow2 # backup minimize
sudo reboot # restart
sudo sed --in-place 's/^#LEDS=/LEDS=/' /etc/kbd/config # enable capslock boot
sudo sed --in-place 's/^mibs/#mibs/' /etc/snmp/snmp.conf # disable
sudo sh -c 'cryptsetup open --type luks /dev/disk/by-uuid/0179e7de-a468-4213-b932-ee2df135336d encrypted && mount /dev/mapper/encrypted /media/encrypted' # mount encrypted disk
sudo sh -c 'dhclient -r wlan0 && dhclient wlan0' # request refresh dhcp ip
sudo sh -c 'ip addr add 192.168.0.99/16 dev wlan0 && dhclient wlan0' # set ip address network
sudo sh -c 'tail --follow=name --retry --lines=0 $(find /var/log/ -type f -exec file -- {} + | rg --regexp=":.*(ASCII|UTF)" | cut --delimiter=: --fields=1)' # text
sudo sh -c 'umount /media/encrypted && cryptsetup close encrypted' # disk unmount
sudo smartctl --attributes /dev/sda # harddisk smart
sudo ss --listening --tcp --numeric --processes
sudo strace -p 123 # process
sudo sysctl --all # list kernel parameters
sudo system-config-printer
sudo systemctl daemon-reload # reload service file
sudo systemctl reboot --firmware # bios uefi
sudo tcpdump # network debug log packets
sudo tcpdump tcp # network debug log packets protocol
sudo tcpdump 'tcp[tcpflags] & (tcp-syn|tcp-fin) != 0 and not src and dst net 127.0.0.1' # network debug log packets internet
sudo traceroute -4 -p 1234 -T example.org # ipv4 port tcp network
sudo umount "/media/mountpoint"
sudo umount ~/mp3
sudo updatedb
sudo update-grub
sudo -u postgres createuser --pwprompt username
sudo -u postgres dropuser username
sudo -u postgres pg_dumpall > backup.sql # postgresql backup
sudo -u postgres pg_dump postgres > backup.sql # postgresql backup
sudo -u postgres psql <<< "COPY(SELECT datname FROM pg_database WHERE datistemplate = FALSE) TO STDOUT;" # list all postgresql
sudo -u postgres psql <<< "COPY(SELECT extract(epoch from now())::Integer) TO STDOUT;" # unix integer timestamp
sudo -u postgres psql --dbname my_db <<< "\dt my_schema.*" # database schema tables list all postgresql
sudo -u postgres psql --dbname postgres < dump.sql # postgresql import
sudo -u postgres psql --dbname postgres # postgresql login interactive
sudo -u postgres psql <<< "\dn" # schemas list all postgresql
sudo -u postgres psql <<< "\dt" # public schema tables list all postgresql
sudo -u postgres psql <<< "\du" # users list all postgresql
sudo -u postgres psql <<< "\encoding" # postgresql db encoding
sudo -u postgres psql <<< "SELECT * FROM pg_stat_activity;" # list sessions processes postgresql
sudo -u postgres psql --single-transaction --file backup.sql # restore postgresql
sudo -u postgres psql --variable name="Robert'); DROP TABLE Students; --" <<< "COPY(SELECT :'name') TO STDOUT;" # test escape literal postgresql
sudo useradd USER # add create user
sudo userdel USER # delete remove user
sudo usermod --append --groups GROUP "$USER" # modify user add group
sudo usermod --groups "$(id --name --groups | sed 's/ \?group \?/ /g;s/ /,/g;s/^,//;s/,$//')" "$USER" # remove group
sudo -u tor arm # anonymizing relay monitor service tor
sudo vigr # edit groups users members
sudo vipw # edit password users
sudo virsh dumpxml NAME # export backup vm config
sudo virsh list --all # vm
sudo visudo # permissions security
sudo wavemon
sudo wifi-menu --obscure
sudo wifi-radar # wifi diagnostic
su --login "$USER" # substitute user login
sum <<< '2 2'
synclient SHMConfig=1 -m 100 | tee synaptics.log # synaptics touchpad debug
systemctl --failed # service status
systemctl is-enabled SERVICE
systemctl list-units # services
systemctl status SERVICE
systemctl status # service status
systemctl --user restart SERVICE
systemd-analyze blame # list boot service time
systemd-analyze # boot time kernel user
systemd-analyze critical-chain # boot process dependency time
systemd-analyze dot | dot -Tsvg > startup-dependencies.svg # 2d startup process dependencies
systemd-analyze plot > startup-timing.svg # time startup process
systemd-analyze security SERVICE
tail --follow=name --retry --lines=0 FILE
tar --create --gzip --exclude-vcs --directory PARENT_DIR --file FILE PATH # compress gzip
tar --extract --gzip --file FILE # decompress gzip
tar --extract --gzip --transform 's#.*/##' --file FILE # decompress flatten gzip
tar --list --gzip --file FILE
telnet localhost 1234 # network
tiff2pdf -o PDF_FILE TIFF_FILE # convert
time bash -lc true # benchmark startup login shell
timedatectl status
timeout 1 sleep 2
tofu apply # deploy
tofu import ADDRESS ID
tofu plan # test
top -c # command line
top -p "$(pgrep -d ',' bash)" # monitor process
torify curl https://check.torproject.org # privacy network tor check
torsocks --shell # privacy tor interactive shell
touch --date='2001-02-03 04:05:06.789' file # set access modification time
touch file # create empty file
tput colors
traceroute HOST # network debugging
traps="$(trap)" # signal
trap # signal
trust list # pkcs#11 policy certificate tls
tshark -D # list network interface
tshark -G fields | rg '\tdns\t' | cut --fields=2-4,6- # list field dns
tshark -n -T fields -e dns.qry.name src port 53 # network dns
tshark -Y 'http.request and http contains "application/ocsp-request"' -T fields -e http.host tcp port 80 # network ocsp request
tty
type -a COMMAND # list command alias builtin function executable
TZ=REGION/NAME date --date='TZ="REGION/NAME" DATETIME' # convert time zone
udevadm info --export-db
udevadm monitor --environment kernel # live events
ulimit -a
ulimit -c unlimited
umask
umask -S # symbolic
umount /mnt/foo # unmount device drive
uname --all
uname --kernel-name --kernel-release --kernel-version --machine --processor --hardware-platform --operating-system # anonymized
unetbootin &
unexpand --tabs=4 FILE # convert space tab
units --terse NCURRENCY CURRENCY # convert currency
unix2dos contacts.vcf # newline convert
unset -f function
unset variable_or_function
unset -v variable
unzip file.zip # decompress zip
unzip -l file.zip # list zip
upower --dump # battery
uptime
usb-devices # list usb device
uuidgen
vagrant box list # template
vagrant box prune # delete old template
vagrant box update # upgrade template
vagrant destroy # delete
vagrant halt # shutdown
vagrant reload # reboot
vagrant snapshot list # backup
vagrant snapshot restore NAME # backup
vagrant snapshot save NAME # backup state
vagrant ssh # login
vagrant status
vagrant up # start boot
vainfo
valgrind foo # check memory binary
. .venv/bin/activate # python
 vim ~/.bash_history # shell
virt-customize --add PATH --root-password password:PASSWORD # vm
virtualbox &
vkcube # vulkan graphics demo
vlc --ffmpeg-hw --verbose 2 file.1080p.x264.mkv # video h.264
vlc --full-help | less
vlc --fullscreen --deinterlace -1 --deinterlace-mode yadif2x --video-filter postproc --postproc-q 6 --audio-language ja --sub-language en --verbose 2 --advanced dvdsimple:///dev/dvd &
vlc --fullscreen --deinterlace -1 --deinterlace-mode yadif2x --video-filter postproc --postproc-q 6 --verbose 2 --advanced ./*.avi
vlc http://www.lynnepublishing.com/surround/www_lynnemusic_com_surround_test.ac3 # audio
vlc --spdif http://www.lynnepublishing.com/surround/www_lynnemusic_com_surround_test.ac3 # audio
vmware
vncpasswd
vncserver -kill :1 # stop service
vncserver -list # all
vncserver -localhost -geometry "$(xrandr | sed --quiet 's/.*current \([0-9]*\) x \([0-9]*\).*/\1x\2/p')" # start service remote desktop
vncviewer :1 # local desktop
vncviewer :2 # remote desktop
vulkaninfo # graphics
w
wait # process pid
watch --color --differences -- git diff --color=always # change
wc --lines -- FILE # line count
wdiff -w "$(tput bold && tput setaf 1)" -x "$(tput sgr0)" -y "$(tput bold && tput setaf 2)" -z "$(tput sgr0)" path1 path2 # color word diff
weechat # irc client
wget --output-document - http://user:password@host/function?id=foo 2>service.log | json_pp # web service
wget --server-response --output-document=/dev/null http://example.org/ # web header
whatis COMMAND # exact help man
which --all bash # list executable file path
while IFS= read -r -d '' -u 9; do if [[ "$(file --brief --special-files --mime-type -- "$REPLY")" = text/* ]]; then sed --in-place 's/[ \t]\+\(\r\?\)$/\1/;$a\' -- "$REPLY"; else echo "Skipping $REPLY" >&2; fi; done 9< <(find . \( -type d -regex '^.*/\.git$' -prune -false \) -o -type f -exec printf '%s\0' {} +) # text whitespace eol eof
while IFS= read -r -d '' -u 9; do printf '%q\n' "${REPLY#* }"; done 9< <(find . -printf '%T@' -exec printf ' %s\0' {} \; | sort --general-numeric-sort --zero-terminated) # sort file list modification date
while IFS= read -r -d '' -u 9; do sed --in-place '/\x0/{q;}; s/[ \t]\+\(\r\?\)$/\1/;$a\' -- "$REPLY"; done 9< <(find . \( -type d -regex '^.*/\.git$' -prune -false \) -o -type f -exec printf '%s\0' {} +) # file whitespace eol eof
while IFS= read -r -u 9; do if [[ "$REPLY" =~ .*\.dot$ ]]; then dot -O -Tsvg "$REPLY"; fi; done 9< <(inotifywait --event close_write --format %f --monitor .)
while read -r; do xdotool windowactivate "$REPLY"; xdotool key F5; done < <(xdotool search --name "Mozilla Firefox") # refresh
while sleep 1; do (shopt -s nullglob; events_dir="${HOME}/.events"; for path in "$events_dir"/*; do notify-send --icon="${HOME}/my projects/graphics/${USER}.png" "$(basename "$path")" "$(cat "$path")" && rm "$path"; done;) done
whoami # user id
whois example.org # dns lookup internet network
who # logins users
wireshark &
xclip FILE # mouse clipboard copy
xclip -out # mouse clipboard print stdout
xclip -selection clipboard FILE # keyboard clipboard copy
xclip -selection clipboard -out # keyboard clipboard print stdout
xdg-open "$(nix eval --raw nixpkgs#nix.doc.outPath)"/share/doc/nix/manual/index.html
xdg-open PATH # email html image http pdf sql
xev # x event keyboard mouse input
xinput # all keyboard device list
xinput --list "$(which-keyboard)" # keyboard device info
xinput --list-props "$(which-keyboard)" # keyboard device properties
xkill # kill x window
xmllint --noout *.xml # validate syntax
xmllint --shell input.xml # interactive shell xpath xml
xmllint --xpath XPATH FILE
xmodmap -pm # list keyboard modifiers
xmodmap -pp # list mouse buttons
xprop | rg --color=never "^(WM_CLASS|WM_NAME)"
xprop # x window properties
xrandr --display :0 --output LVDS1 --mode 1366x768 # reset resolution graphics desktop
xrandr --verbose # graphics hardware
xrdb -query # list
xset q # list settings x monitor
xsltproc --output file.xml file.xslt file.xml # transform xslt xml replace
xsltproc --output file.xml ~/my\ projects/xml/filter/filter.xslt file.xml # filter xml xslt replace
xwininfo -id "$(xdotool selectwindow)"
xwininfo -id "$(xprop -root | awk '/_NET_ACTIVE_WINDOW\(WINDOW\)/{print $NF}')" # current window
xxd "$(which xxd)" | head --lines=1 # binary hex
(yes a & yes b) | cat >/dev/null & ~/my\ projects/pspipe/fdpid.sh 0 "$!" # process pid pipe stdin
yes -- STRING # repeat string
yq --inplace '(PATH) = VALUE' FILE # modify yaml
yt-dlp --continue --cookies-from-browser=firefox --output='%(uploader)s/%(playlist|)s/%(upload_date)s-%(title)s-%(id)s.%(ext)s' --path="${HOME}/videos" --sub-langs='en.*' URL # download video files
yt-dlp --continue --embed-thumbnail --format=bestaudio --output='%(upload_date)s-%(title)s-%(id)s.%(ext)s' --path="${HOME}/music" URL # download audio files thumbnail
zip --update file.zip input # add file compress zip
zless /proc/config.gz # kernel configuration parameter
