# shellcheck shell=bash
# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#set -o xtrace -o errexit # Debug
set -o noclobber -o nounset -o pipefail

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth:erasedups

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
HISTFILESIZE=100000

# Push the prompt to the bottom of the terminal
tput cup 999 0

# Make sure all terminals save history
PROMPT_COMMAND='history -a; history -c; history -r'

git() {
    if [[ "$*" == 'root' ]]; then
        local root
        root="$(git rev-parse --show-toplevel)" || return $?
        cd "${root}" || return $?
    else
        command git "$@"
    fi
}

grep() {
    command grep --color=auto "$@"
}

# direnv <https://direnv.net/>
direnv_hook="$(direnv hook bash)"
eval "${direnv_hook}"
unset direnv_hook

if [[ -r "${HOME}/.bashrc_local" ]]; then
    # shellcheck source=/dev/null
    source "${HOME}/.bashrc_local"
fi

set +o noclobber +o nounset +o pipefail +o histexpand

true
