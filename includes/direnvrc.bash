: "${XDG_CACHE_HOME:="${HOME}/.cache"}"
declare -A direnv_layout_dirs
direnv_layout_dir() {
    local hash path raw_hash
    raw_hash="$(sha1sum - <<< "${PWD}")"
    hash="$(cut --delimiter=' ' --fields=1 <<< "${raw_hash}")"
    path="${PWD//[^a-zA-Z0-9]/-}"
    echo "${direnv_layout_dirs[${PWD}]:=${XDG_CACHE_HOME}/direnv/layouts/${hash}${path}}"
}
