{
  lib,
  pkgs,
  ...
}:
{
  imports = lib.filesystem.listFilesRecursive ../modules;
  fileSystems."/".device = "/dev/mapper/vg-root";
  networking.domain = "example.org";
  nixpkgs.config.allowUnfreePredicate =
    pkg:
    (builtins.elem (lib.getName pkg) (
      map lib.getName [
        pkgs._7zz
        pkgs.discord
        pkgs.dwarf-fortress
        pkgs.jetbrains.rust-rover
        pkgs.obsidian
        pkgs.spotify
        pkgs.starsector
        pkgs.steam
        pkgs.steam-unwrapped
        pkgs.unrar
      ]
    ));
  system.stateVersion = "24.11";
}
