{
  inputs = {
    home-manager = {
      inputs.nixpkgs.follows = "nixpkgs";
      url = "github:nix-community/home-manager";
    };
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    sops-nix = {
      inputs.nixpkgs.follows = "nixpkgs";
      url = "github:Mic92/sops-nix";
    };
  };
  outputs =
    {
      home-manager,
      nixpkgs,
      sops-nix,
      ...
    }:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
    in
    {
      checks.${system} = import ./tests { inherit pkgs; };
      devShells.${system}.default = import ./shell.nix { inherit pkgs system; };
      nixosModules = {
        android-backup = import ./modules/android-backup.nix;
        defaults = import ./modules/defaults.nix;
        dev = import ./modules/dev.nix;
        disabled-service-config-warnings = import ./modules/disabled-service-config-warnings.nix;
        external-webcam = import ./modules/external-webcam.nix;
        gaming = import ./modules/gaming.nix;
        gui = import ./modules/gui.nix;
        nix-index = import ./modules/nix-index.nix;
        pijul = import ./modules/pijul.nix;
        photography = import ./modules/photography.nix;
        ssh-client = import ./modules/ssh-client.nix;
        ssh-server = import ./modules/ssh-server.nix;
        thunderbird = import ./modules/thunderbird.nix;
        tor = import ./modules/tor.nix;
        xdg = import ./modules/xdg.nix;
      };
      nixosConfigurations.ci = nixpkgs.lib.nixosSystem {
        inherit system;
        modules = [
          home-manager.nixosModules.home-manager
          sops-nix.nixosModules.sops

          (import ./.ci/dummy-configuration.nix)
        ];
      };
    };
}
