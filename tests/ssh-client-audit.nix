{ pkgs }:
let
  nixosLib = import (pkgs.path + "/nixos/lib") { };

  sshKeys = import (pkgs.path + "/nixos/tests/ssh-keys.nix") pkgs;
  sshUsername = "any-user";
  machineName = "machine";
  sshAuditPort = 2222;
in
nixosLib.runTest {
  hostPkgs = pkgs;

  name = "ssh-client-audit";

  nodes.${machineName} = {
    imports = [
      ../modules/ssh-client.nix
      ../modules/ssh-server.nix
    ];
    networking.firewall.allowedTCPPorts = [ sshAuditPort ];
    system.stateVersion = "24.11";
    users.users.${sshUsername} = {
      isNormalUser = true;
      openssh.authorizedKeys.keys = [ sshKeys.snakeOilPublicKey ];
    };
  };

  testScript = ''
    ${machineName}.start()

    # Wait for SSH daemon service to start
    ${machineName}.wait_for_unit("sshd.service")

    # Set up trusted private key
    ${machineName}.succeed("cat ${sshKeys.snakeOilPrivateKey} > privkey.snakeoil")
    ${machineName}.succeed("chmod 600 privkey.snakeoil")

    # Fail fast and disable interactivity
    ssh_options = "-o BatchMode=yes -o ConnectTimeout=10 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"

    # Should allow non-root user certificate login
    ${machineName}.wait_until_succeeds(f"ssh {ssh_options} -i privkey.snakeoil ${sshUsername}@${machineName} true")

    # Should deny root user
    ${machineName}.fail(f"ssh {ssh_options} root@${machineName} true")
    ${machineName}.wait_until_succeeds("journalctl --unit=sshd --lines=1 | grep --fixed-strings --line-buffered --quiet 'Connection closed by invalid user root'")

    # Should deny non-root user password login
    ${machineName}.fail(f"ssh {ssh_options} -o PasswordAuthentication=yes ${sshUsername}@${machineName} true")
    ${machineName}.wait_until_succeeds("journalctl --unit=sshd --lines=1 | grep --fixed-strings --line-buffered --quiet 'Connection closed by authenticating user ${sshUsername}'")

    # Should pass SSH client audit
    service_name = "ssh-audit.service"
    ${machineName}.succeed(f"systemd-run --unit={service_name} ${pkgs.ssh-audit}/bin/ssh-audit --client-audit --port=${toString sshAuditPort}")
    ${machineName}.sleep(5) # We can't use wait_for_open_port because ssh-audit exits as soon as anything talks to it
    ${machineName}.execute(
        f"ssh {ssh_options} -i privkey.snakeoil -p ${toString sshAuditPort} ${sshUsername}@${machineName} true",
        check_return=False,
        timeout=10
    )
    ${machineName}.succeed(f"exit $(systemctl show --property=ExecMainStatus --value {service_name})")
  '';
}
