{ pkgs }:
let
  nixosLib = import (pkgs.path + "/nixos/lib") { };

  machineName = "machine";
in
nixosLib.runTest {
  hostPkgs = pkgs;

  name = "fontconfig-dtd";

  nodes.${machineName} = {
    environment.systemPackages = [ pkgs.libxml2 ];
  };

  testScript = ''
    ${machineName}.start()

    ${machineName}.succeed("xmllint --noout --dtdvalid 'file://${pkgs.fontconfig.out}/share/xml/fontconfig/fonts.dtd' ${../includes/fontconfig-local.xml}")
  '';
}
