name="$(git config user.name)"
email="$(git config user.email)"

if [[ $# -eq 0 ]]; then
    set -- "${name} <${email}>"
fi

for co_author; do
    trailers+=(--trailer "Co-Authored-By=${co_author}")
done

git commit --amend --no-edit "${trailers[@]}"
