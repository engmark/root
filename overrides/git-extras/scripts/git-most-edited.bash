set -o pipefail

git log --diff-filter=M --name-status --pretty=format: "$@" | sed '/^$/d' | cut --fields=2- | sort | uniq --count | sort --numeric-sort --reverse
