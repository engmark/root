git-filter-clean(1)
===================

NAME
----
git-filter-clean - Apply new smudge filters

SYNOPSIS
--------
[verse]
'git filter-clean'

DESCRIPTION
-----------
Forces Git to re-apply smudge filters to the entire repository, by checking
out the current commit.

Based on <https://stackoverflow.com/a/21653524/96588>.

SEE ALSO
--------
linkgit:git-checkout[1]
linkgit:git-stash[1]

BUGS
----
Please report bugs at <https://gitlab.com/engmark/root/-/issues>.
