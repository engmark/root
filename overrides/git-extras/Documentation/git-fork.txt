git-fork(1)
===========

NAME
----
git-fork - Set up a clone with a connection to the upstream repo

SYNOPSIS
--------
[verse]
'git fork' <upstream> <downstream>

DESCRIPTION
-----------
Clone a repository with the original repository as the "upstream"
remote, and your repository as the "origin" remote.

EXAMPLES
--------
Fork this repository into your own GitLab account under username `myuser`::
+
------------
$ git fork git@gitlab.com:engmark/root.git git@gitlab.com:myuser/root.git
------------

SEE ALSO
--------
linkgit:git-clone[1]
linkgit:git-remote[1]

BUGS
----
Please report bugs at <https://gitlab.com/engmark/root/-/issues>.
