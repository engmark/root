{
  system ? builtins.currentSystem,
  pkgs ? import <nixpkgs> { inherit system; },
}:
pkgs.mkShell {
  packages = [
    pkgs.asciidoctor
    pkgs.bashInteractive
    pkgs.bash-language-server
    pkgs.cacert
    pkgs.check-jsonschema
    pkgs.dconf2nix
    pkgs.deadnix
    pkgs.editorconfig-checker
    pkgs.gitFull
    pkgs.gitlint
    pkgs.libxml2
    pkgs.luaformatter
    pkgs.lua-language-server
    pkgs.marksman
    pkgs.nix
    pkgs.nixf
    pkgs.nixfmt-rfc-style
    pkgs.nixos-rebuild
    pkgs.nil
    pkgs.nodePackages.prettier
    pkgs.pre-commit
    pkgs.python3Packages.jsonschema
    pkgs.shellcheck
    pkgs.shfmt
    pkgs.statix
    pkgs.yq-go
  ];
  env = {
    LOCALE_ARCHIVE = "${pkgs.glibcLocales}/lib/locale/locale-archive";
  };
}
