{ config, pkgs, ... }:
{
  boot = {
    extraModprobeConfig = builtins.readFile ../includes/external-webcam-modprobe.conf;
    extraModulePackages = [ config.boot.kernelPackages.v4l2loopback.out ];
    kernelModules = [ config.boot.kernelPackages.v4l2loopback.pname ];
  };

  environment.systemPackages = [
    pkgs.v4l-utils # V4L utils and libv4l
  ];

  programs.gphoto2.enable = true; # Creates "camera" group

  services.udev.extraRules = builtins.readFile ../includes/external-webcam.rules;

  systemd.services.external-webcam = {
    enableStrictShellChecks = true;
    path = [
      pkgs.ffmpeg-full
      pkgs.gphoto2
    ];
    script = ''
      ${pkgs.gphoto2}/bin/gphoto2 --stdout --capture-movie |
        ${pkgs.ffmpeg-full}/bin/ffmpeg -i - -vcodec rawvideo -pix_fmt yuv420p -f v4l2 /dev/video0
    '';
    serviceConfig = {
      CapabilityBoundingSet = "";
      DynamicUser = true;
      LockPersonality = true;
      NoNewPrivileges = true;
      ProtectClock = true;
      ProtectHostname = true;
      PrivateNetwork = true;
      ProtectHome = true;
      ProtectKernelLogs = true;
      ProtectKernelModules = true;
      ProtectKernelTunables = true;
      RestrictNamespaces = true;
      SupplementaryGroups = [
        "camera" # To read camera stream
        "video" # To write to /dev/video*
      ];
      SystemCallArchitectures = "native";
      SystemCallFilter = "~@clock @cpu-emulation @debug @module @mount @obsolete @privileged @raw-io @reboot @resources @swap";
      UMask = "0777";
    };
  };

  users.users.victor.extraGroups = [
    "video" # To read from /dev/video*
  ];
}
