{
  pkgs,
  ...
}:
{
  environment = {
    systemPackages = [
      pkgs.crosswords # Crossword player and editor
      pkgs.daggerfall-unity # Open source recreation of Daggerfall in the Unity engine
      # pkgs.dosbox-staging
      # pkgs.dreamweb # 2D point-and-click cyberpunk top-down adventure game
      # pkgs.dwarf-fortress-packages.dwarf-fortress-full
      pkgs.er-patcher # Enhancement patches for Elden Ring adding ultrawide support, custom frame rate limits and more
      # pkgs.flight-of-the-amazon-queen
      # pkgs.freeciv_gtk
      # pkgs.gamehub # Unified game library
      # pkgs.gnome-mahjongg # Disassemble a pile of tiles by removing matching pairs
      # pkgs.gzdoom # DOOM engine
      # pkgs.heroic # Native GOG, Epic, and Amazon Games Launcher
      # pkgs.hhexen
      # pkgs.jazz2 # Jazz Jackrabbit 2
      # pkgs.lugaru # Third person ninja rabbit fighting game
      # pkgs.lure-of-the-temptress
      # pkgs.lutris-free
      pkgs.mangohud # Overlay for monitoring FPS, temperatures, CPU/GPU load and more
      # pkgs.nethack-qt
      pkgs.nexusmods-app-unfree # Game mod manager
      # pkgs.openmw # Morrowind engine
      # pkgs.openra # Command and Conquer engine; TODO: Enable once https://github.com/NixOS/nixpkgs/issues/360335 is fixed
      # pkgs.openttd
      # pkgs.opentyrian # Tyrian port
      # pkgs.openxcom
      # pkgs.playonlinux # Game manager
      # pkgs.pokerth # Online poker
      # pkgs.prismlauncher # Minecraft mod manager
      # pkgs.rpcs3 # PS3 emulator
      pkgs.scarab # Hollow Knight mod manager
      pkgs.shadps4 # PS4 emulator
      # pkgs.shattered-pixel-dungeon # Roguelike
      # pkgs.starsector # Space-combat, roleplaying, exploration, and economic game
      # pkgs.stuntrally # Stunt Rally game with track editor
      # pkgs.superTuxKart # 3D kart racing game
      # pkgs.teeworlds # Retro multiplayer shooter
      # pkgs.steamtinkerlaunch # Steam client wrapper tool
      # pkgs.the-powder-toy # 2D physics sandbox game
      # pkgs.torcs # The Open Racing Car Simulator
      # pkgs.typespeed # Typing game
      # pkgs.ultimatestunts # Remake of Stunts
      # pkgs.uqm # Ur-Quan Masters
      # pkgs.vdrift # Driving simulation
      # pkgs.wesnoth # The Battle for Wesnoth, a free, turn-based strategy game with a fantasy theme
      # pkgs.xmoto
    ];
    variables.ENABLE_GAMESCOPE_WSI = "1";
  };

  hardware = {
    bluetooth.settings = {
      General.Privacy = "device";
      LE = {
        ConnectionLatency = 0;
        MaxConnectionInterval = 9;
        MinConnectionInterval = 7;
      };
    };
    xpadneo.enable = true;
  };

  programs = {
    gamemode = {
      enable = true;
      settings = {
        custom = {
          start = "${pkgs.libnotify}/bin/notify-send --category=presence.online --expire-time=2000 --urgency=low 'GameMode started'";
          end = "${pkgs.libnotify}/bin/notify-send --category=presence.offline --expire-time=2000 --urgency=low 'GameMode ended'";
        };
        general.softrealtime = "auto";
        gpu = {
          amd_performance_level = "high";
          apply_gpu_optimisations = "accept-responsibility";
          gpu_device = 0;
        };
      };
    };
    gamescope.enable = true;
    steam = {
      enable = true;
      extraCompatPackages = [ pkgs.proton-ge-bin ];
      gamescopeSession = {
        args = [ "--hdr-enabled" ];
        enable = true;
      };
    };
  };
}
