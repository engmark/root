{ config, ... }:
{
  home-manager.users.victor =
    { pkgs, ... }:
    {
      programs.thunderbird = {
        enable = true;
        package = pkgs.thunderbird.override {
          extraPolicies.ExtensionSettings = {
            "marcoagpinto@mail.telepac.pt" = {
              install_url = "https://addons.thunderbird.net/thunderbird/downloads/latest/british-english-dictionary-2/addon-461570-latest.xpi";
              installation_mode = "force_installed";
            };
            "fr-dicollecte@dictionaries.addons.mozilla.org" = {
              install_url = "https://addons.thunderbird.net/thunderbird/downloads/latest/dictionnaire-fran%C3%A7ais1/addon-354872-latest.xpi";
              installation_mode = "force_installed";
            };
            "de-DE@dictionaries.addons.mozilla.org" = {
              install_url = "https://addons.thunderbird.net/thunderbird/downloads/latest/dictionary-german/addon-3077-latest.xpi";
              installation_mode = "force_installed";
            };
            "de-CH@dictionaries.addons.mozilla.org" = {
              install_url = "https://addons.thunderbird.net/thunderbird/downloads/latest/dictionary-german-swiss/addon-3049-latest.xpi";
              installation_mode = "force_installed";
            };
            "en-NZ-new@dictionaries.addons.mozilla.org" = {
              install_url = "https://addons.thunderbird.net/thunderbird/downloads/latest/new-zealand-english/addon-444420-latest.xpi";
              installation_mode = "force_installed";
            };
            "nb-NO@dictionaries.addons.mozilla.org" = {
              install_url = "https://addons.thunderbird.net/thunderbird/downloads/latest/norsk-bokm%C3%A5l-ordliste/addon-6868-latest.xpi";
              installation_mode = "force_installed";
            };
          };
        };
        profiles.personal = {
          isDefault = true;
          search = {
            engines = {
              "Bing".metaData.hidden = true;
              "Google".metaData.hidden = true;
              "Wikipedia (en)".metaData.hidden = true;
            };
            force = true; # Overwrite search.json.mozlz4 when building
          };
        };
        settings = {
          "app.donation.eoy.version.viewed" = 99; # Disable donation spam for a while
          "calendar.alarms.eventalarmlen" = 1;
          "calendar.alarms.eventalarmunit" = "hours";
          "calendar.alarms.onforevents" = 1;
          "calendar.alarms.onfortodos" = 1;
          "calendar.alarms.todoalarmlen" = 1;
          "calendar.alarms.todoalarmunit" = "hours";
          "calendar.timezone.useSystemTimezone" = true;
          "calendar.week.start" = 1; # Monday
          "datareporting.healthreport.uploadEnabled" = false;
          "datareporting.policy.dataSubmissionPolicyAcceptedVersion" = 2;
          "mail.compose.autosaveinterval" = 1; # Minutes
          "mail.compose.big_attachments.notify" = false;
          "mail.identity.default.archive_granularity" = 0; # Single folder
          "mail.identity.default.archives_folder_picker_mode" = "1"; # Set manually
          "mail.identity.default.drafts_folder_picker_mode" = "0"; # Use default
          "mail.identity.default.fcc_folder_picker_mode" = "1"; # Set manually
          "mail.identity.default.fcc_reply_follows_parent" = true; # Put replies in same folder as original
          "mail.identity.default.reply_on_top" = 1; # Above quoted message
          "mail.identity.default.sig_bottom" = true; # Below quoted message
          "mail.identity.default.tmpl_folder_picker_mode" = "0"; # Use default
          "mail.purge.ask" = false;
          "mail.rights.version" = 1;
          "mail.server.default.moveOnSpam" = true; # Move detected spam
          "mail.spam.manualMark" = true; # Move manually marked spam
          "mail.warn_on_send_accel_key" = false;
          "mailnews.emptyTrash.dontAskAgain" = true;
          "network.cookie.cookieBehavior" = 3;
          "privacy.donottrackheader.enabled" = true;
          "spellchecker.dictionary" = builtins.concatStringsSep "," [
            "de-CH"
            "en-GB"
            "en-NZ"
            "nb-NO"
          ];
        };
      };
    };
  xdg.mime.defaultApplications."x-scheme-handler/mailto" =
    config.programs.thunderbird.package.desktopItem.name;
}
