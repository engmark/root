{
  config,
  lib,
  pkgs,
  ...
}:
let
  imports = [ ./external-webcam.nix ];
in
{
  inherit imports;

  environment = {
    gnome.excludePackages = [
      pkgs.epiphany
      pkgs.geary
      pkgs.gnome-contacts
      pkgs.gnome-music
      pkgs.gnome-tour
      pkgs.totem
    ];
    systemPackages = [
      (pkgs.aspellWithDicts (dicts: [
        dicts.de
        dicts.en
        dicts.en-computers
        dicts.fr
        dicts.mi
        dicts.nb
      ]))
      pkgs.calibre # E-book software
      pkgs.clinfo # OpenCL platforms and devices info
      pkgs.diffpdf # Diff pdf files visually or textually
      pkgs.discord # Voice and text chat
      pkgs.element-desktop # Client for Matrix.org
      pkgs.evince # Document viewer
      pkgs.fd # Alternative to find
      pkgs.ffmpeg-full # Record, convert and stream audio and video
      pkgs.gimp # GNU Image Manipulation Program
      pkgs.gnome-tweaks # Customize GNOME 3
      pkgs.gnomeExtensions.emoji-copy # Find and copy emojis
      pkgs.gnomeExtensions.panel-world-clock-lite # Show multiple clocks
      pkgs.gparted # Disk partitioning tool
      pkgs.graphviz # Graph visualization
      pkgs.httrack # Website mirroring utility
      (pkgs.hunspellWithDicts [
        pkgs.hunspellDicts.de-ch
        pkgs.hunspellDicts.de-de
        pkgs.hunspellDicts.en-gb-large
        pkgs.hunspellDicts.fr-moderne
        pkgs.hunspellDicts.nb-no
      ]) # Spell checker
      pkgs.imagemagick # Create, edit, compose, or convert bitmap images
      pkgs.inkscape # Vector graphics editor
      pkgs.jetbrains.idea-community # Java, Kotlin, Groovy and Scala IDE from jetbrains
      pkgs.jetbrains.rust-rover # Rust IDE from jetbrains
      pkgs.keepassxc # Offline password manager
      pkgs.krita # Painting application
      pkgs.libnotify # Send desktop notifications
      pkgs.libreoffice # Office suite
      pkgs.mcomix # Comic book reader and image viewer
      pkgs.mesa-demos # Demos and test programs for OpenGL and Mesa
      pkgs.nautilus # File manager
      pkgs.obsidian # Knowledge base
      pkgs.onlyoffice-bin # Office suite
      pkgs.pavucontrol # PulseAudio Volume Control
      pkgs.qbittorrent # BitTorrent client
      pkgs.signal-desktop # Messenger
      pkgs.simple-scan # Scanning utility
      pkgs.shotcut # Video editor
      pkgs.spotify # Spotify client
      pkgs.streamlink # Extract streams from various websites to a video player
      pkgs.telegram-desktop # Desktop messaging
      pkgs.ungoogled-chromium # Browser
      pkgs.vivaldi-ffmpeg-codecs # Proprietary codecs for Vivaldi
      pkgs.vlc # Media player and streaming server
      pkgs.watchmate # PineTime smart watch companion app
      pkgs.xclip # Access the X clipboard
      pkgs.xdotool # Fake keyboard/mouse input, window management, etc
      pkgs.xorg.xdpyinfo # Display information about an X server
      pkgs.xorg.xhost # Add and delete host names or user names to the list allowed to make connections to the X server
      pkgs.xorg.xkill # Instruct the X server to forcefully terminate its connection to a client
      pkgs.xorg.xmodmap # Modify keymaps and pointer button mappings
      pkgs.xournalpp # PDF annotation
      pkgs.yt-dlp # Videos downloader
    ];
  };

  fonts = {
    fontconfig = {
      localConf = builtins.readFile ../includes/fontconfig-local.xml;
      useEmbeddedBitmaps = true;
    };
    packages = [
      (pkgs.nerd-fonts.jetbrains-mono or (pkgs.nerdfonts.override { fonts = [ "JetBrainsMono" ]; }))
      pkgs.noto-fonts
      pkgs.stix-two
      pkgs.twemoji-color-font
    ];
  };

  hardware = {
    graphics.enable = true;
  } // lib.optionalAttrs (config.hardware ? pulseaudio) { pulseaudio.enable = false; };

  home-manager.users.victor =
    { config, nixosConfig, ... }:
    {
      dconf.settings =
        let
          inherit (lib.gvariant)
            mkDictionaryEntry
            mkDouble
            mkEmptyArray
            mkInt32
            mkTuple
            mkUint32
            mkVariant
            type
            ;

          locations = [
            (mkVariant (mkTuple [
              (mkUint32 2)
              (mkVariant (mkTuple [
                "Coordinated Universal Time (UTC)"
                "@UTC"
                false
                (mkEmptyArray "(dd)")
                (mkEmptyArray "(dd)")
              ]))
            ]))
            (mkVariant (mkTuple [
              (mkUint32 2)
              (mkVariant (mkTuple [
                "London"
                "EGWU"
                true
                [
                  (mkTuple [
                    (mkDouble "0.8997172294030767")
                    (mkDouble "-7.272211034407213e-3")
                  ])
                ]
                [
                  (mkTuple [
                    (mkDouble "0.8988445647770796")
                    (mkDouble "-2.0362232784242244e-3")
                  ])
                ]
              ]))
            ]))
            (mkVariant (mkTuple [
              (mkUint32 2)
              (mkVariant (mkTuple [
                "Oslo"
                "ENGM"
                true
                [
                  (mkTuple [
                    (mkDouble "1.0506882097005865")
                    (mkDouble "0.19344065294494067")
                  ])
                ]
                [
                  (mkTuple [
                    (mkDouble "1.0457431159710333")
                    (mkDouble "0.1876228945893904")
                  ])
                ]
              ]))
            ]))
            (mkVariant (mkTuple [
              (mkUint32 2)
              (mkVariant (mkTuple [
                "Sydney"
                "YSSY"
                true
                [
                  (mkTuple [
                    (mkDouble "-0.592539281052075")
                    (mkDouble "2.638646934988996")
                  ])
                ]
                [
                  (mkTuple [
                    (mkDouble "-0.5913757223996479")
                    (mkDouble "2.639228723041856")
                  ])
                ]
              ]))
            ]))
            (mkVariant (mkTuple [
              (mkUint32 2)
              (mkVariant (mkTuple [
                "Auckland"
                "NZAA"
                true
                [
                  (mkTuple [
                    (mkDouble "-0.64606271726433173")
                    (mkDouble "3.0508355324860883")
                  ])
                ]
                [
                  (mkTuple [
                    (mkDouble "-0.64344472338634029")
                    (mkDouble "3.0502537618865206")
                  ])
                ]
              ]))
            ]))
          ];
        in
        {
          "org/gnome/clocks" = {
            world-clocks = map (location: [ (mkDictionaryEntry "location" location) ]) locations;
          };

          "org/gnome/desktop/interface" = {
            clock-format = "24h";
            clock-show-weekday = true;
            overlay-scrolling = false;
          };

          "org/gnome/desktop/search-providers" = {
            disabled = [
              "org.gnome.Characters.desktop"
              "org.gnome.Contacts.desktop"
            ];
          };

          "org/gnome/desktop/wm/keybindings" = {
            move-to-workspace-1 = mkEmptyArray type.string;
            move-to-workspace-2 = mkEmptyArray type.string;
            move-to-workspace-3 = mkEmptyArray type.string;
            move-to-workspace-4 = mkEmptyArray type.string;
            move-to-workspace-5 = mkEmptyArray type.string;
            move-to-workspace-6 = mkEmptyArray type.string;
            move-to-workspace-7 = mkEmptyArray type.string;
            move-to-workspace-8 = mkEmptyArray type.string;
            move-to-workspace-9 = mkEmptyArray type.string;
            move-to-workspace-10 = mkEmptyArray type.string;
            move-to-workspace-11 = mkEmptyArray type.string;
            move-to-workspace-12 = mkEmptyArray type.string;
            move-to-workspace-left = mkEmptyArray type.string;
            move-to-workspace-right = mkEmptyArray type.string;
            move-to-workspace-up = mkEmptyArray type.string;
            move-to-workspace-down = mkEmptyArray type.string;
            move-to-workspace-last = mkEmptyArray type.string;
            switch-to-workspace-1 = mkEmptyArray type.string;
            switch-to-workspace-2 = mkEmptyArray type.string;
            switch-to-workspace-3 = mkEmptyArray type.string;
            switch-to-workspace-4 = mkEmptyArray type.string;
            switch-to-workspace-5 = mkEmptyArray type.string;
            switch-to-workspace-6 = mkEmptyArray type.string;
            switch-to-workspace-7 = mkEmptyArray type.string;
            switch-to-workspace-8 = mkEmptyArray type.string;
            switch-to-workspace-9 = mkEmptyArray type.string;
            switch-to-workspace-10 = mkEmptyArray type.string;
            switch-to-workspace-11 = mkEmptyArray type.string;
            switch-to-workspace-12 = mkEmptyArray type.string;
            switch-to-workspace-left = mkEmptyArray type.string;
            switch-to-workspace-right = mkEmptyArray type.string;
            switch-to-workspace-up = mkEmptyArray type.string;
            switch-to-workspace-down = mkEmptyArray type.string;
            switch-to-workspace-last = mkEmptyArray type.string;
          };

          "org/gnome/desktop/wm/preferences" = {
            num-workspaces = mkInt32 1;
          };

          "org/gnome/file-roller/listing" = {
            list-mode = "as-folder";
            show-path = false;
            sort-method = "name";
            sort-type = "ascending";
          };

          "org/gnome/nautilus/list-view" = {
            default-zoom-level = "small";
          };

          "org/gnome/nautilus/preferences" = {
            default-folder-viewer = "list-view";
          };

          "org/gnome/settings-daemon/plugins/media-keys" = {
            custom-keybindings = [
              "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/"
            ];
            home = [ "<Super>e" ];
            magnifier = [ "<Alt><Super>8" ];
            magnifier-zoom-in = [ "<Alt><Super>equal" ];
            magnifier-zoom-out = [ "<Alt><Super>minus" ];
            mic-mute = [ "<Shift><Control><Super>m" ];
            next = [ "<Ctrl><Super>Right" ];
            play = [ "<Ctrl><Super>space" ];
            playback-forward = [ "<Shift><Ctrl><Super>Right" ];
            playback-rewind = [ "<Shift><Ctrl><Super>Left" ];
            previous = [ "<Ctrl><Super>Left" ];
            screensaver = [ "<Super>l" ];
            volume-down-precise = [ "<Ctrl><Super>Down" ];
            volume-mute = [ "<Ctrl><Super>m" ];
            volume-up-precise = [ "<Ctrl><Super>Up" ];
          };

          "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0" = {
            binding = "<Ctrl><Alt>t";
            command = "kgx";
            name = "GNOME Console";
          };

          "org/gnome/shell" = {
            # Get extension IDs from `gnome-extensions list`
            enabled-extensions = [
              "emoji-copy@felipeftn"
              "world_clock_lite@ailin.nemui"
            ];
            favorite-apps = [
              "org.gnome.Console.desktop"
              config.programs.firefox.package.desktopItem.name
              pkgs.obsidian.desktopItem.name
              "org.gnome.Nautilus.desktop"
              nixosConfig.programs.thunderbird.package.desktopItem.name
              "org.telegram.desktop.desktop"
              pkgs.element-desktop.desktopItem.name
              pkgs.discord.desktopItem.name
              "org.keepassxc.KeePassXC.desktop"
              pkgs.jetbrains.idea-community.desktopItem.name
              "org.darktable.darktable.desktop"
              "org.kde.digikam.desktop"
              "spotify.desktop"
              "calibre-gui.desktop"
              "org.qbittorrent.qBittorrent.desktop"
            ];
          };

          "org/gnome/shell/extensions/world-clock" = {
            active-buttons = [
              "@UTC"
              "EGWU"
              "ENGM"
              "YSSY"
              "NZAA"
            ];
          };

          "org/gnome/shell/weather" = {
            automatic-location = true;
            locations = [
              (mkVariant (mkTuple [
                (mkUint32 2)
                (mkVariant (mkTuple [
                  "Zürich"
                  "LSZH"
                  true
                  [
                    (mkTuple [
                      (mkDouble "0.8287405006708767")
                      (mkDouble "0.1489347570190853")
                    ])
                  ]
                  [
                    (mkTuple [
                      (mkDouble "0.8267042948457449")
                      (mkDouble "0.1492256510455152")
                    ])
                  ]
                ]))
              ]))
              (mkVariant (mkTuple [
                (mkUint32 2)
                (mkVariant (mkTuple [
                  "Wellington"
                  "NZWN"
                  true
                  [
                    (mkTuple [
                      (mkDouble "-0.7214027516732254")
                      (mkDouble "3.0508355324860883")
                    ])
                  ]
                  [
                    (mkTuple [
                      (mkDouble "-0.720820981073658")
                      (mkDouble "3.050544638459658")
                    ])
                  ]
                ]))
              ]))
            ];
          };

          "org/gnome/shell/window-switcher" = {
            current-workspace-only = true;
          };

          "org/gnome/shell/world-clocks" = {
            inherit locations;
          };

          "org/gnome/simple-scan" = {
            save-directory = "file://${config.home.homeDirectory}/documents/";
          };

          "org/gnome/software" = {
            first-run = false;
          };

          "org/gnome/tweaks" = {
            show-extensions-notice = false;
          };

          "org/gtk/settings/file-chooser" = {
            show-hidden = false;
            sort-directories-first = true;
            sort-order = "ascending";
          };
        };
      fonts.fontconfig = {
        defaultFonts = {
          emoji = [
            "Noto Color Emoji"
            "Twitter Color Emoji"
          ];
          monospace = [
            "JetBrainsMonoNL Nerd Font"
            "Noto Sans Mono"
            "DejaVu Sans Mono"
          ];
          sansSerif = [
            "Noto Sans"
            "DejaVu Sans"
          ];
          serif = [
            "Noto Serif"
            "DejaVu Serif"
          ];
        };
        enable = true;
      };
      home.file = {
        ".XCompose".source = ../includes/.XCompose;
        "idea.properties".text = "idea.filewatcher.executable.path = ${pkgs.fsnotifier}/bin/fsnotifier";
      };
      programs = {
        feh = {
          enable = true;
          keybindings = {
            delete = "Delete";
            remove = "C-Delete";
          };
        };
        firefox = {
          enable = true;
          package = pkgs.firefox-esr; # Required for some policies - see https://mozilla.github.io/policy-templates/
          policies = {
            DisableFirefoxStudies = true;
            DisablePocket = true;
            DisableTelemetry = true;
            DisplayBookmarksToolbar = "always";
            DisplayMenuBar = "never";
            DontCheckDefaultBrowser = true;
            OverrideFirstRunPage = "";
            OverridePostUpdatePage = "";
            PasswordManagerEnabled = false;
            SearchBar = "unified";
            EnableTrackingProtection = {
              Value = true;
              Locked = true;
              Cryptomining = true;
              Fingerprinting = true;
            };
            ExtensionSettings = {
              "*".installation_mode = "blocked";
              "chrome-gnome-shell@gnome.org" = {
                # Don't install GNOME Shell integration; use pkgs.gnomeExtensions.* instead
                install_url = "https://addons.mozilla.org/firefox/downloads/latest/gnome-shell-integration/latest.xpi";
                installation_mode = "blocked";
              };
              "copy-selected-tabs-to-clipboard@piro.sakura.ne.jp" = {
                # Copy Selected Tabs to Clipboard
                install_url = "https://addons.mozilla.org/firefox/downloads/latest/copy-selected-tabs-to-clipboar/latest.xpi";
                installation_mode = "normal_installed";
              };
              "en-NZ@dictionaries.addons.mozilla.org" = {
                # New Zealand English Dictionary
                install_url = "https://addons.mozilla.org/firefox/downloads/latest/new-zealand-english-dict/latest.xpi";
                installation_mode = "normal_installed";
              };
              "idcac-pub@guus.ninja" = {
                # I still don't care about cookies
                install_url = "https://addons.mozilla.org/firefox/downloads/latest/istilldontcareaboutcookies/latest.xpi";
                installation_mode = "normal_installed";
              };
              "jid1-MnnxcxisBPnSXQ@jetpack" = {
                # Privacy Badger
                install_url = "https://addons.mozilla.org/firefox/downloads/latest/privacy-badger17/latest.xpi";
                installation_mode = "normal_installed";
              };
              "jid1-tfBgelm3d4bLkQ@jetpack" = {
                # Copy as Markdown
                install_url = "https://addons.mozilla.org/firefox/downloads/latest/copy_as_markdown/latest.xpi";
                installation_mode = "normal_installed";
              };
              "tineye@ideeinc.com" = {
                # TinEye Reverse Image Search
                install_url = "https://addons.mozilla.org/firefox/downloads/latest/tineye-reverse-image-search/latest.xpi";
                installation_mode = "normal_installed";
              };
              "uBlock0@raymondhill.net" = {
                # uBlock Origin
                install_url = "https://addons.mozilla.org/firefox/downloads/latest/ublock-origin/latest.xpi";
                installation_mode = "normal_installed";
              };
              "wayback_machine@mozilla.org" = {
                # Wayback Machine
                install_url = "https://addons.mozilla.org/firefox/downloads/latest/wayback-machine_new/latest.xpi";
                installation_mode = "normal_installed";
              };
              "{d187b435-812e-4813-a93e-edccc4118f9d}" = {
                # British English Dictionary
                install_url = "https://addons.mozilla.org/firefox/downloads/latest/british-english-dictionary-gb/latest.xpi";
                installation_mode = "normal_installed";
              };
              "{da90161a-9c5c-4315-adae-2eedbe24810a}" = {
                # Insecure Links Highlighter
                install_url = "https://addons.mozilla.org/firefox/downloads/latest/insecure-links-highlighter/latest.xpi";
                installation_mode = "normal_installed";
              };
              "{ddc62400-f22d-4dd3-8b4a-05837de53c2e}" = {
                # Read Aloud: A Text to Speech Voice Reader
                install_url = "https://addons.mozilla.org/firefox/downloads/latest/read-aloud/latest.xpi";
                installation_mode = "normal_installed";
              };
            };
            Handlers = {
              mimeTypes."application/pdf".action = "saveToDisk";
            };
            InstallAddonsPermission = {
              Allow = [ "https://addons.mozilla.org" ];
              Default = true;
            };
            Permissions = {
              Autoplay = {
                Default = "block-audio-video";
                Locked = true;
              };
              Location = {
                BlockNewRequests = true;
                Locked = true;
              };
            };
            PopupBlocking.Default = true;
            Preferences = {
              "accessibility.typeaheadfind.flashBar" = 0;
              "app.normandy.api_url" = "";
              "app.normandy.enabled" = false; # Telemetry subsystem
              "app.normandy.first_run" = false;
              "app.shield.optoutstudies.enabled" = false;
              "browser.aboutConfig.showWarning" = false; # Don't warn about about:config safety
              "browser.contentblocking.category" = "strict";
              "browser.discovery.enabled" = false;
              "browser.display.use_document_fonts" = 0;
              "browser.download.autohideButton" = false;
              "browser.download.panel.shown" = true;
              "browser.download.useDownloadDir" = true;
              "browser.engagement.ctrlTab.has-used" = true;
              "browser.engagement.downloads-button.has-used" = true;
              "browser.engagement.fxa-toolbar-menu-button.has-used" = true;
              "browser.engagement.home-button.has-used" = true;
              "browser.engagement.library-button.has-used" = true;
              "browser.engagement.sidebar-button.has-used" = true;
              "browser.firefox-view.feature-tour" = builtins.toJSON {
                complete = true;
                message = "FIREFOX_VIEW_FEATURE_TOUR";
                screen = "";
              };
              "browser.laterrun.enabled" = false;
              "browser.messaging-system.whatsNewPanel.enabled" = false;
              "browser.newtabpage.activity-stream.asrouter.userprefs.cfr.addons" = false;
              "browser.newtabpage.activity-stream.asrouter.userprefs.cfr.features" = false;
              "browser.newtabpage.activity-stream.feeds.section.topstories" = false;
              "browser.newtabpage.activity-stream.feeds.snippets" = false;
              "browser.newtabpage.activity-stream.feeds.telemetry" = false;
              "browser.newtabpage.activity-stream.feeds.topsites" = false;
              "browser.newtabpage.activity-stream.section.highlights.includeBookmarks" = false;
              "browser.newtabpage.activity-stream.section.highlights.includeDownloads" = false;
              "browser.newtabpage.activity-stream.section.highlights.includePocket" = false;
              "browser.newtabpage.activity-stream.section.highlights.includeVisited" = false;
              "browser.newtabpage.activity-stream.showSponsored" = false;
              "browser.newtabpage.activity-stream.showSponsoredTopSites" = false;
              "browser.newtabpage.activity-stream.system.showSponsored" = false;
              "browser.newtabpage.activity-stream.telemetry" = false;
              "browser.newtabpage.enabled" = false;
              "browser.newtabpage.pinned" = builtins.toJSON [ ];
              "browser.ping-centre.telemetry" = false;
              "browser.protections_panel.infoMessage.seen" = true;
              "browser.rights.3.shown" = true;
              "browser.safebrowsing.downloads.remote.enabled" = false;
              "browser.search.suggest.enabled" = false;
              "browser.shopping.experience2023.enabled" = false;
              "browser.startup.homepage_override.mstone" = "ignore";
              "browser.startup.page" = 3; # Resume
              "browser.tabs.firefox-view.mobilePromo.dismissed" = true;
              "browser.toolbars.bookmarks.visibility" = "always";
              "browser.uitour.enabled" = false;
              "browser.urlbar.addons.featureGate" = false;
              "browser.urlbar.mdn.featureGate" = false;
              "browser.urlbar.pocket.featureGate" = false;
              "browser.urlbar.quicksuggest.scenario" = "history";
              "browser.urlbar.speculativeConnect.enabled" = false;
              "browser.urlbar.suggest.engines" = false;
              "browser.urlbar.suggest.quicksuggest.nonsponsored" = false;
              "browser.urlbar.suggest.quicksuggest.sponsored" = false;
              "browser.urlbar.suggest.searches" = false;
              "browser.urlbar.trending.featureGate" = false;
              "browser.urlbar.weather.featureGate" = false;
              "browser.warnOnQuitShortcut" = false;
              "datareporting.healthreport.uploadEnabled" = false;
              "datareporting.policy.dataSubmissionEnabled" = false;
              "datareporting.policy.dataSubmissionPolicyAcceptedVersion" = 2;
              "devtools.everOpened" = true;
              "devtools.performance.new-panel-onboarding" = false;
              "distribution.iniFile.exists.value" = true;
              "doh-rollout.doneFirstRun" = true;
              "dom.private-attribution.submission.enabled" = false; # https://mzl.la/3Xht2j1
              "extensions.htmlaboutaddons.recommendations.enabled" = false;
              "extensions.pocket.enabled" = false;
              "extensions.recommendations.hideNotice" = true;
              "extensions.ui.dictionary.hidden" = false;
              "extensions.ui.locale.hidden" = false;
              "extensions.ui.plugin.hidden" = false;
              "extensions.ui.sitepermission.hidden" = false;
              "extensions.update.autoUpdateDefault" = false;
              "extensions.webextensions.ExtensionStorageIDB.migrated.jid1-MnnxcxisBPnSXQ@jetpack" = true;
              "extensions.webextensions.ExtensionStorageIDB.migrated.uBlock0@raymondhill.net" = true;
              "extensions.webextensions.ExtensionStorageIDB.migrated.wayback_machine@mozilla.org" = true;
              "findbar.highlightAll" = true;
              "general.smoothScroll" = true;
              "identity.fxaccounts.toolbar.accessed" = true;
              "intl.regional_prefs.use_os_locales" = true;
              "media.autoplay.default" = 5; # Disable
              "pdfjs.enableScripting" = false;
              "pref.downloads.disable_button.edit_actions" = false;
              "pref.privacy.disable_button.cookie_exceptions" = false;
              "privacy.annotate_channels.strict_list.enabled" = true;
              "privacy.donottrackheader.enabled" = true;
              "privacy.fingerprintingProtection" = true;
              "privacy.globalprivacycontrol.enabled" = true;
              "privacy.globalprivacycontrol.was-ever-enabled" = true;
              "privacy.partition.network_state.ocsp_cache" = true;
              "privacy.popups.showBrowserMessage" = false;
              "privacy.query_stripping.enabled" = true;
              "privacy.query_stripping.enabled.pbmode" = true;
              "privacy.resistFingerprinting" = true;
              "privacy.trackingprotection.emailtracking.enabled" = true;
              "privacy.trackingprotection.enabled" = true;
              "privacy.trackingprotection.socialtracking.enabled" = true;
              "privacy.userContext.enabled" = true;
              "services.sync.declinedEngines" = "passwords";
              "services.sync.engine.passwords" = false;
              "signon.rememberSignons" = false;
              "toolkit.coverage.endpoint.base" = "";
              "toolkit.coverage.opt-out" = true;
              "toolkit.telemetry.archive.enabled" = false;
              "toolkit.telemetry.bhrPing.enabled" = false;
              "toolkit.telemetry.coverage.opt-out" = true;
              "toolkit.telemetry.enabled" = false;
              "toolkit.telemetry.firstShutdownPing.enabled" = false;
              "toolkit.telemetry.newProfilePing.enabled" = false;
              "toolkit.telemetry.pioneer-new-studies-available" = false;
              "toolkit.telemetry.reportingpolicy.firstRun" = false;
              "toolkit.telemetry.server" = "data:";
              "toolkit.telemetry.shutdownPingSender.enabled" = false;
              "toolkit.telemetry.unified" = false;
              "toolkit.telemetry.updatePing.enabled" = false;
              "trailhead.firstrun.didSeeAboutWelcome" = true;
              "widget.gtk.overlay-scrollbars.enabled" = false;
            };
            SearchEngines = {
              Add = [
                {
                  Name = "Nix functions";
                  URLTemplate = "https://noogle.dev/q?term={searchTerms}";
                  Method = "GET";
                  IconURL = "https://noogle.dev/favicon.png";
                  Alias = "nix";
                }
                {
                  Name = "NixOS options";
                  URLTemplate = "https://search.nixos.org/options?channel=unstable&query={searchTerms}";
                  Method = "GET";
                  IconURL = "https://search.nixos.org/favicon.png";
                  Alias = "nixos";
                }
                {
                  Name = "Nix packages";
                  URLTemplate = "https://search.nixos.org/packages?channel=unstable&query={searchTerms}";
                  Method = "GET";
                  IconURL = "https://search.nixos.org/favicon.png";
                  Alias = "nixpkgs";
                }
                {
                  Name = "Nix packages";
                  URLTemplate = "https://www.nixhub.io/search?q={searchTerms}";
                  Method = "GET";
                  IconURL = "https://www.nixhub.io/favicon.ico";
                  Alias = "old-nixpkgs";
                }
              ];
              PreventInstalls = true;
              Remove = [
                "Amazon.com"
                "Bing"
                "Google"
                "Wikipedia (en)"
              ];
            };
            UserMessaging = {
              ExtensionRecommendations = false; # Don’t recommend extensions while the user is visiting web pages
              FeatureRecommendations = false; # Don’t recommend browser features
              Locked = true; # Prevent the user from changing user messaging preferences
              MoreFromMozilla = false; # Don’t show the “More from Mozilla” section in Preferences
              SkipOnboarding = true; # Don’t show onboarding messages on the new tab page
              UrlbarInterventions = false; # Don’t offer suggestions in the URL bar
              WhatsNew = false; # Remove the “What’s New” icon and menuitem
            };
          };
        };
        mpv = {
          config = {
            save-position-on-quit = true;
            write-filename-in-watch-later-config = true;
          };
          enable = true;
          extraInput = ''
            g script-message-to seek_to toggle-seeker
          '';
          scripts = [
            pkgs.mpvScripts.inhibit-gnome
            pkgs.mpvScripts.seekTo
          ] ++ lib.optionals (pkgs.mpvScripts ? youtube-chat) [ pkgs.mpvScripts.youtube-chat ];
        };
      };
      xdg.configFile =
        let
          editableSymlink = path: {
            force = true;
            source = config.lib.file.mkOutOfStoreSymlink "${config.home.homeDirectory}/my projects/root//includes/${path}";
          };
        in
        lib.attrsets.genAttrs [
          "darktable/darktablerc"
          "darktable/keyboardrc"
          "darktable/shortcutsrc"
          "darktable/shortcutsrc.defaults"
          "darktable/styles/Canon-7D.dtstyle"
          "darktable/styles/Enhanced-color-profile-on-Rec2020-display.dtstyle"
          "digikam_systemrc"
          "digikamrc"
          "keepassxc/keepassxc.ini"
          "mcomix/keybindings.conf"
          "mcomix/preferences.conf"
        ] editableSymlink;
    };

  nixpkgs.overlays = [
    (_final: prev: {
      gnome-console = prev.gnome-console.overrideAttrs (old: {
        patches = (old.patches or [ ]) ++ [
          ../patches/disable-gnome-console-close-window-prompt-${old.version}.patch
        ];
      });
    })
  ];

  programs = {
    nautilus-open-any-terminal.enable = true;
    obs-studio = {
      enable = true;
      enableVirtualCamera = true;
    };
    ydotool.enable = true;
  };

  security = {
    polkit.extraConfig = builtins.readFile ../includes/disable-login-suspend.rules;
    rtkit.enable = true;
  };

  services = {
    flatpak.enable = true;
    gnome = {
      gnome-keyring.enable = true;
      gnome-online-accounts.enable = true;
      localsearch.enable = false;
    };
    libinput.enable = true;
    pipewire = {
      enable = true;
      jack.enable = true;
      wireplumber.configPackages = [
        (pkgs.writeTextDir "share/wireplumber/wireplumber.conf.d/51-disable-suspension.conf" (
          builtins.readFile ../includes/51-disable-suspension.conf
        ))
      ];
    };
    xserver = {
      desktopManager.gnome.enable = true;
      displayManager.gdm = {
        autoSuspend = false;
        enable = true;
      };
      enable = true;
      xkb = {
        layout = "us";
        options = "compose:caps";
        variant = "dvorak-alt-intl,";
      };
    };
  } // lib.optionalAttrs (config.services ? pulseaudio) { pulseaudio.enable = false; };

  users.users.victor.extraGroups = [
    config.users.groups.audio.name # See https://nixos.wiki/wiki/PulseAudio#Enabling_PulseAudio
    config.users.groups.ydotool.name
  ];
}
