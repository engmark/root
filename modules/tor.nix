{
  pkgs,
  ...
}:
{
  environment.systemPackages = [
    pkgs.tor-browser # Privacy-focused browser routing traffic through the Tor network
  ];

  services.tor.client.enable = true;
}
