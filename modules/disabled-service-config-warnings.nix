# TODO: Build recursive version of the below, maybe with lib.attrsets.mapAttrsRecursiveCond
# TODO: Check entire configuration, not just services
{
  config,
  lib,
  options,
  ...
}:
let
  getServiceWarnings =
    serviceName: serviceOptions:
    let
      getOptionWarning =
        optionName: option:
        if
          (builtins.tryEval (
            option.isDefined or true
            && option ? default
            && (config.services.${serviceName}.${optionName} or null) != (option.default or null)
          )).value
        then
          "config.services.${serviceName}.enable = false && config.services.${serviceName}.${optionName} != options.services.${serviceName}.${optionName}.default"
        else
          null;
    in
    if (builtins.tryEval config.services.${serviceName}.enable or true).value then
      [ ]
    else
      lib.attrsets.mapAttrsToList getOptionWarning serviceOptions;

  unbrokenServices = lib.attrsets.removeAttrs options.services [
    "nagios" # error: cannot coerce null to a string: null
    "part-db" # function 'generic' called with unexpected argument 'extraConfig'
    "redis" # Renaming error: option `services.redis.servers."".enable' does not exist.
    "webdav-server-rs" # error: attribute 'webdav' missing
  ];
in
{
  warnings = lib.lists.remove null (
    lib.lists.flatten (lib.attrsets.mapAttrsToList getServiceWarnings unbrokenServices)
  );
}
