{
  config,
  lib,
  pkgs,
  options,
  ...
}:
{
  console = {
    colors = [
      "073642"
      "dc322f"
      "859900"
      "b58900"
      "268bd2"
      "d33682"
      "2aa198"
      "eee8d5"
      "002b36"
      "cb4b16"
      "586e75"
      "657b83"
      "839496"
      "6c71c4"
      "93a1a1"
      "fdf6e3"
    ];
    earlySetup = true; # Set virtual console options in initrd
    useXkbConfig = true; # Configure the virtual console keymap from the xserver keyboard settings
  };

  documentation = {
    dev.enable = true;
    #man.generateCaches = true; # TODO: Reintroduce when https://github.com/NixOS/nixpkgs/issues/384499 is resolved
    nixos.includeAllModules = true;
  };

  environment = {
    systemPackages = [
      pkgs.age # Encryption tool
      pkgs.aha # Ansi HTML Adapter
      pkgs.bc # Shell calculator
      pkgs.binutils # Manipulate binaries
      pkgs.btop # Resource monitor
      pkgs.cachix # Nix binary cache hosting
      pkgs.chars # Display information about unicode characters
      pkgs.coreutils-full # GNU Core Utilities
      pkgs.davfs2 # Mount WebDAV shares like a typical filesystem
      pkgs.devenv # Nix developer environments
      pkgs.diffoscope # Compare files, archives, and directories
      pkgs.dmidecode # List system hardware info
      pkgs.dos2unix # Convert line breaks
      pkgs.duf # Disk Usage/Free Utility
      pkgs.exfat # exFAT file system
      pkgs.file # Identify file types
      pkgs.findutils # GNU Find Utilities
      pkgs.gdb # GNU debugger
      pkgs.gnupg # GNU Privacy Guard
      pkgs.groff # GNU Troff, a typesetting package
      pkgs.hddtemp # Display hard disk temperature
      pkgs.hyperfine # Benchmarking tool
      pkgs.iftop # Display bandwidth usage on a network interface
      pkgs.inotify-info # track down the number of inotify watches, instances, and which files are being watched
      pkgs.inxi # System information tool
      pkgs.iotop # I/O process monitoring
      pkgs.jq # JSON processor
      pkgs.libxslt # XSL transformations tools
      pkgs.linux-manual # Linux kernel API manual pages
      pkgs.lm_sensors # Read hardware sensors
      pkgs.lshw # Hardware configuration info
      pkgs.lsof # List open files
      pkgs.man # Standard Unix documentation system
      pkgs.man-pages # Linux development manual pages
      pkgs.man-pages-posix # POSIX manual pages
      pkgs.moreutils # Tool collection including sponge
      pkgs.mtr # Network diagnostics tool combining ping and traceroute
      pkgs.ncdu # Disk usage analyzer
      pkgs.netcat-gnu # Read and write data across network connections
      pkgs.nix-diff # Compare .drv derivation files
      pkgs.nix-du # Determine which gc-roots take space in your nix store
      pkgs.nix-tree # Browse a Nix store path's dependencies
      pkgs.nixfmt-rfc-style # Official formatter for Nix code
      pkgs.nixpkgs-review # Review nixpkgs pull requests
      pkgs.nmap # Network discovery and security auditing
      pkgs.nvme-cli # NVM-Express user space tooling
      pkgs.odt2txt # ODT to TXT converter
      pkgs.openvpn # Tunneling application
      pkgs.oxipng # PNG optimizer
      pkgs.p7zip # Compressor
      pkgs.pandoc # Convert between documentation formats
      (lib.hiPrio pkgs.parallel-full) # Replace moreutils' parallel command
      pkgs.parted # Create, destroy, resize, check, and copy partitions
      pkgs.patch # GNU Patch
      pkgs.pciutils # Inspect and manipulate configuration of PCI devices
      pkgs.pijul # Distributed version control system
      pkgs.psmisc # Proc filesystem utils such as fuser, killall and pstree
      pkgs.qpdf # Inspect and manipulate PDF files
      pkgs.qprint # Encode and decode Quoted-Printable files
      pkgs.ranger # File manager
      pkgs.rclone # Sync files with cloud storage
      pkgs.ripgrep # DWIM grep
      pkgs.rsync # File transfer utility
      pkgs.scc # Code counter with complexity calculations and COCOMO estimates
      pkgs.semgrep # Static analysis for many languages
      pkgs.sops # Secrets OPerationS; tool for managing secrets
      pkgs.sox # Sample Rate Converter for audio
      pkgs.ssh-to-age # Convert SSH private keys in ED25519 format to age keys
      pkgs.sshfs # FUSE-based filesystem that allows remote filesystems to be mounted over SSH
      pkgs.strace # System call tracer
      pkgs.sysstat # Performance monitoring tools
      pkgs.tcpdump # Network sniffer
      pkgs.traceroute # Track the route taken by packets over an IP network
      pkgs.units # Unit conversion tool
      pkgs.unrar # Utility for RAR archives
      pkgs.unzip # Extraction utility for archives compressed in ZIP format
      pkgs.watchexec # Execute commands in response to file modifications
      pkgs.wget # Retrieve files using HTTP, HTTPS, and FTP
      pkgs.whois # WHOIS client
      pkgs.yq-go # YAML processor
      pkgs.zip # Compressor/archiver for creating and modifying ZIP files
    ];
  };

  home-manager.users.victor =
    { pkgs, ... }:
    {
      programs.helix = {
        enable = true;
        languages.language = [
          {
            name = "nix";
            auto-format = true;
            formatter.command = lib.getExe pkgs.nixfmt-rfc-style;
          }
        ];
      };
    };

  nixpkgs.overlays = [
    (_final: prev: { parallel-full = prev.parallel-full.override { willCite = true; }; })
  ];

  programs = {
    adb.enable = true;
    bash.interactiveShellInit = builtins.readFile ../includes/bashrc.bash;
    direnv = {
      direnvrcExtra = builtins.readFile ../includes/direnvrc.bash;
      enable = true;
    };
    gnupg.agent.enable = true;
    neovim = {
      configure = {
        customRC = builtins.readFile ../includes/init.vim;
        packages.myVimPackage = {
          start = [
            pkgs.vimPlugins.nvim-treesitter.withAllGrammars
            pkgs.vimPlugins.undotree
            pkgs.vimPlugins.vim-lsp
          ];
        };
      };
      defaultEditor = true;
      enable = true;
      viAlias = true;
      vimAlias = true;
    };
    nix-ld.enable = true;
    starship = {
      enable = true;
      settings = {
        shlvl = {
          disabled = false;
          threshold = 1;
        };
        status.disabled = false;
      };
    };
    wireshark.enable = true;
  };

  security.sudo = {
    execWheelOnly = true;
    extraConfig = ''
      # 1 hour between password prompts
      Defaults timestamp_timeout=60
    '';
  };

  services.locate.enable = true;

  systemd = {
    enableStrictShellChecks = true;
    services = {
      packagekit.enable = false;
    };
    user.services.add-ssh-key = {
      inherit (config.systemd.user.services.ssh-agent) unitConfig wantedBy;
      bindsTo = [ "ssh-agent.service" ];
      environment.SSH_AUTH_SOCK = "/run/user/%U/ssh-agent";
      path = [ options.programs.ssh.package.value ];
      script = "${options.programs.ssh.package.value}/bin/ssh-add";
      serviceConfig = {
        CapabilityBoundingSet = "";
        LockPersonality = true;
        NoNewPrivileges = true;
        ProtectClock = true;
        ProtectHostname = true;
        PrivateNetwork = true;
        ProtectKernelLogs = true;
        ProtectKernelModules = true;
        ProtectKernelTunables = true;
        RestrictAddressFamilies = "AF_UNIX";
        RestrictNamespaces = true;
        SystemCallArchitectures = "native";
        SystemCallFilter = "~@clock @cpu-emulation @debug @module @mount @obsolete @privileged @raw-io @reboot @resources @swap";
        UMask = "0777";
      };
    };
  };

  users.users.victor = {
    extraGroups = [
      config.users.groups.adbusers.name # For programs.adb
      config.users.groups.wireshark.name # For programs.wireshark
    ];
    isNormalUser = true;
  };

  virtualisation.docker = {
    autoPrune = {
      dates = "daily";
      flags = [
        "--all"
        "--volumes"
      ];
    };
    enable = true;
    rootless = {
      enable = true;
      setSocketVariable = true;
    };
  };
}
