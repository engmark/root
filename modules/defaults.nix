{
  config,
  lib,
  modulesPath,
  pkgs,
  ...
}:
{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  boot.loader = {
    efi.canTouchEfiVariables = true;
    systemd-boot.enable = true;
  };

  environment = {
    defaultPackages = lib.mkForce [ ];
    systemPackages = [
      pkgs.extundelete # Recover deleted files from an ext3 or ext4 partition
      pkgs.ntfs3g # FUSE-based NTFS driver with full write support
    ];
  };

  hardware = {
    bluetooth.enable = true;
    sane.enable = true;
  };

  home-manager.users.victor =
    { config, nixosConfig, ... }:
    {
      home = {
        file =
          let
            editableSymlink = path: {
              force = true;
              source = config.lib.file.mkOutOfStoreSymlink "${config.home.homeDirectory}/my projects/root//includes/${path}";
            };
          in
          {
            ".bash_history" = editableSymlink ".bash_history";
            ".editorconfig".source = ../includes/.editorconfig;
          };
        stateVersion = "24.05";
      };
      programs = {
        git = {
          aliases = {
            authors = "shortlog --email --summary"; # List contributors ordered by name
            current-branch = "symbolic-ref --short HEAD"; # Print currently checked-out branch
            default-remote = "config --default=origin checkout.defaultRemote"; # Print default remote name
            local-only-log = "log --branches --not --remotes --simplify-by-decoration --decorate --oneline"; # List commits not on any remote
            st = "status";
          };
          attributes = [
            "*.bash diff=bash"
            "*.htm diff=html"
            "*.html diff=html"
            "*.java diff=java"
            "*.jpeg diff=jpeg"
            "*.jpg diff=jpeg"
            "*.js diff=javascript"
            "*.markdown diff=markdown"
            "*.md diff=markdown"
            "*.odp diff=odt"
            "*.ods diff=odt"
            "*.odt diff=odt"
            "*.ott diff=odt"
            "*.pdf diff=pdf"
            "*.php diff=php"
            "*.pl diff=perl"
            "*.py diff=python"
            "*.rake diff=ruby"
            "*.rb diff=ruby"
            "*.rs diff=rust"
            "*.tex diff=tex"
            "*.ts diff=javascript"
            "*.xhtml diff=html"
            "GraphicsConfig.xml diff=utf16le"
          ];
          enable = true;
          extraConfig = {
            branch = {
              autoSetupMerge = "simple";
              autoSetupRebase = "always";
              sort = "-committerdate";
            };
            commit = {
              gpgsign = true;
              verbose = true;
            };
            core.untrackedCache = true;
            diff = {
              algorithm = "histogram";
              colorMoved = "plain";
              mnemonicPrefix = true;
              "jpeg" = {
                cachetextconv = true;
                textconv = "exif";
              };
              "odt".textconv = "odt2txt";
              "pdf".command = ''diffpdf "$LOCAL" "$REMOTE"'';
              "utf16le".textconv = "iconv --from-code=utf-16 --to-code=utf-8";
              "sops-decrypt".textconv = "sops decrypt";
            };
            difftool."idea".cmd = ''idea-community diff "$LOCAL" "$REMOTE"'';
            fetch = {
              prune = true;
              pruneTags = true;
            };
            gui = {
              diffContext = 3;
              usettk = 0;
            };
            init.defaultBranch = "main";
            log.date = "iso";
            merge = {
              conflictstyle = "zdiff3";
              keepbackup = false;
              tool = "idea";
            };
            mergetool."idea" = {
              cmd = ''idea-community merge "$LOCAL" "$REMOTE" "$BASE" "$MERGED"'';
              trustExitCode = true;
            };
            pull.rebase = true;
            push = {
              autoSetupRemote = true;
              default = "current";
              followTags = true;
            };
            rebase = {
              autosquash = true;
              autostash = true;
            };
            rerere = {
              # "Reuse recorded resolution of conflicted merges"
              autoUpdate = true;
              enabled = true;
            };
            tag.sort = "version:refname";
            user.signingkey = builtins.toString ../includes/id_ed25519.pub;
          };
          ignores = [
            ".idea/"
            "/.attach_pid*"
          ];
          package = import ../overrides/git-extras {
            package = pkgs.git.override {
              guiSupport = true;
            };
          };
          signing = {
            format = "ssh";
            key = builtins.toString (
              pkgs.writeText "allowed_signers" ''
                ${config.programs.git.userEmail} namespaces="git" ${lib.removeSuffix "\n" (builtins.readFile config.programs.git.extraConfig.user.signingkey)}
              ''
            );
            signByDefault = true;
          };
          userEmail = "${config.home.username}@${nixosConfig.networking.domain}";
          userName = nixosConfig.users.users.victor.description;
        };
        readline = {
          bindings = {
            # Up and down arrows search through the history for the characters before the cursor
            "\\e[A" = "history-search-backward";
            "\\e[B" = "history-search-forward";
          };
          enable = true;
          variables = {
            colored-completion-prefix = true; # Enable coloured highlighting of completions
            completion-ignore-case = true; # Auto-complete files with the wrong case
            completion-query-items = 350; # Ask if completion finds more items than this
            match-hidden-files = false;
            page-completions = false;
            revert-all-at-newline = true; # Don't save edited commands until run
            show-all-if-ambiguous = true;
            visible-stats = true; # Show extra information when completing
          };
        };
      };
    };

  i18n.supportedLocales = [ "all" ];

  networking = {
    networkmanager = {
      enable = true;
      ensureProfiles = {
        environmentFiles = [ config.sops.secrets.wifi.path ];
        profiles =
          {
            home = {
              connection = {
                id = "$HOME_SSID";
                type = "wifi";
              };
              wifi.ssid = "$HOME_SSID";
              wifi-security = {
                key-mgmt = "wpa-psk";
                psk = "$HOME_PASSWORD";
              };
            };
          }
          // (builtins.listToAttrs (
            map (
              id:
              lib.nameValuePair id {
                connection = {
                  autoconnect-priority = lib.toInt id; # Connect to later SSIDs first
                  id = "$SSID_${id}";
                  type = "wifi";
                };
                wifi.ssid = "$SSID_${id}";
                wifi-security = {
                  key-mgmt = "wpa-psk";
                  psk = "$PASSWORD_${id}";
                };
              }
            ) (map (number: builtins.toString number) (lib.range 1 27))
          ));
      };
    };
    resolvconf.dnsExtensionMechanism = false;
    stevenblack.enable = true;
    useDHCP = false;
  };

  nix = {
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 30d";
    };
    settings = {
      experimental-features = [
        "flakes"
        "nix-command"
      ];
      download-buffer-size = 500000000; # 500 MB
      trusted-users = [
        config.users.users.root.name
        "@${config.users.groups.wheel.name}"
      ];
    };
  };

  programs.fuse.userAllowOther = true;

  services = {
    acpid.enable = true;
    avahi = {
      enable = true;
      nssmdns4 = true;
      nssmdns6 = true;
    };
    fail2ban.enable = true;
    fwupd.enable = true;
    geoclue2.enableDemoAgent = lib.mkForce true;
    gvfs.enable = true;
    printing = {
      cups-pdf.enable = true;
      enable = true;
    };
    tzupdate.enable = true;
  };

  sops = {
    age = {
      keyFile = "${config.users.users.victor.home}/.config/sops/age/keys.txt";
      sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
    };
    defaultSopsFile = ../secrets/default.yaml;
    secrets = {
      wifi = { };
    };
  };

  users.users.victor.extraGroups = [
    config.users.groups.lp.name # See https://nixos.wiki/wiki/Scanners#Installing_scanner_support
    config.users.groups.networkmanager.name # For managing network connections
    config.users.groups.scanner.name # For https://nixos.wiki/wiki/Scanners#Installing_scanner_support
    config.users.groups.wheel.name # For sudo
  ];
}
