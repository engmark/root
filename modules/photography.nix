{
  config,
  pkgs,
  ...
}:
{
  environment.systemPackages = [
    pkgs.darktable # Virtual lighttable and darkroom for photographers
    pkgs.digikam # Photo management application
    pkgs.exif # Read and manipulate EXIF data in photographs
    pkgs.hugin # Toolkit for stitching photographs and assembling panoramas
    pkgs.jhead # Exif Jpeg header manipulation tool
  ];

  systemd.services = {
    "notify-${config.users.users.victor.name}@" = {
      # Needs `onFailure = [ "notify-${config.users.users.victor.name}@${config.systemd.services.SERVICE.name}" ];` in relevant services
      enableStrictShellChecks = true;
      environment.DBUS_SESSION_BUS_ADDRESS = "unix:path=/run/user/${builtins.toString config.users.users.victor.uid}/bus";
      script = ''
        ${pkgs.libnotify}/bin/notify-send --app-name="$1" 'Service failed - see logs'
      '';
      scriptArgs = "%i";
      serviceConfig.User = config.users.users.victor.name;
    };
    purge-missing-pictures = {
      enableStrictShellChecks = true;
      onFailure = [
        "notify-${config.users.users.victor.name}@${config.systemd.services.purge-missing-pictures.name}"
      ];
      path = [
        pkgs.darktable
        pkgs.procps
        pkgs.sqlite
      ];
      script = ''
        if [[ -e ~/pictures ]] && [[ -e ~/.config/darktable/library.db ]]; then
          ${pkgs.darktable}/share/darktable/tools/purge_non_existing_images.sh --purge
        fi
      '';
      serviceConfig = {
        CapabilityBoundingSet = "";
        LockPersonality = true;
        NoNewPrivileges = true;
        ProtectClock = true;
        ProtectHostname = true;
        PrivateNetwork = true;
        ProtectKernelLogs = true;
        ProtectKernelModules = true;
        ProtectKernelTunables = true;
        RestrictAddressFamilies = "none";
        RestrictNamespaces = true;
        SystemCallArchitectures = "native";
        SystemCallFilter = "~@clock @cpu-emulation @debug @module @mount @obsolete @privileged @raw-io @reboot @resources @swap";
        UMask = "0777";
        User = config.users.users.victor.name;
      };
      startAt = "daily";
    };
  };
}
