{
  lib,
  ...
}:
{
  home-manager.users.victor =
    {
      config,
      nixosConfig,
      pkgs,
      ...
    }:
    let
      projectRoot = "${config.home.homeDirectory}/my projects/root/";
      editableSymlink = path: {
        force = true;
        source = config.lib.file.mkOutOfStoreSymlink "${projectRoot}/includes/${path}";
      };
    in
    {
      dconf.settings =
        let
          inherit (lib.gvariant)
            mkDictionaryEntry
            mkDouble
            mkEmptyArray
            mkInt32
            mkTuple
            mkUint32
            mkVariant
            type
            ;

          locations = [
            (mkVariant (mkTuple [
              (mkUint32 2)
              (mkVariant (mkTuple [
                "Coordinated Universal Time (UTC)"
                "@UTC"
                false
                (mkEmptyArray "(dd)")
                (mkEmptyArray "(dd)")
              ]))
            ]))
            (mkVariant (mkTuple [
              (mkUint32 2)
              (mkVariant (mkTuple [
                "London"
                "EGWU"
                true
                [
                  (mkTuple [
                    (mkDouble "0.8997172294030767")
                    (mkDouble "-7.272211034407213e-3")
                  ])
                ]
                [
                  (mkTuple [
                    (mkDouble "0.8988445647770796")
                    (mkDouble "-2.0362232784242244e-3")
                  ])
                ]
              ]))
            ]))
            (mkVariant (mkTuple [
              (mkUint32 2)
              (mkVariant (mkTuple [
                "Oslo"
                "ENGM"
                true
                [
                  (mkTuple [
                    (mkDouble "1.0506882097005865")
                    (mkDouble "0.19344065294494067")
                  ])
                ]
                [
                  (mkTuple [
                    (mkDouble "1.0457431159710333")
                    (mkDouble "0.1876228945893904")
                  ])
                ]
              ]))
            ]))
            (mkVariant (mkTuple [
              (mkUint32 2)
              (mkVariant (mkTuple [
                "Sydney"
                "YSSY"
                true
                [
                  (mkTuple [
                    (mkDouble "-0.592539281052075")
                    (mkDouble "2.638646934988996")
                  ])
                ]
                [
                  (mkTuple [
                    (mkDouble "-0.5913757223996479")
                    (mkDouble "2.639228723041856")
                  ])
                ]
              ]))
            ]))
            (mkVariant (mkTuple [
              (mkUint32 2)
              (mkVariant (mkTuple [
                "Auckland"
                "NZAA"
                true
                [
                  (mkTuple [
                    (mkDouble "-0.64606271726433173")
                    (mkDouble "3.0508355324860883")
                  ])
                ]
                [
                  (mkTuple [
                    (mkDouble "-0.64344472338634029")
                    (mkDouble "3.0502537618865206")
                  ])
                ]
              ]))
            ]))
          ];
        in
        {
          "org/gnome/clocks" = {
            world-clocks = map (location: [ (mkDictionaryEntry "location" location) ]) locations;
          };

          "org/gnome/desktop/interface" = {
            clock-format = "24h";
            clock-show-weekday = true;
            overlay-scrolling = false;
          };

          "org/gnome/desktop/search-providers" = {
            disabled = [
              "org.gnome.Characters.desktop"
              "org.gnome.Contacts.desktop"
            ];
          };

          "org/gnome/desktop/wm/keybindings" = {
            move-to-workspace-1 = mkEmptyArray type.string;
            move-to-workspace-2 = mkEmptyArray type.string;
            move-to-workspace-3 = mkEmptyArray type.string;
            move-to-workspace-4 = mkEmptyArray type.string;
            move-to-workspace-5 = mkEmptyArray type.string;
            move-to-workspace-6 = mkEmptyArray type.string;
            move-to-workspace-7 = mkEmptyArray type.string;
            move-to-workspace-8 = mkEmptyArray type.string;
            move-to-workspace-9 = mkEmptyArray type.string;
            move-to-workspace-10 = mkEmptyArray type.string;
            move-to-workspace-11 = mkEmptyArray type.string;
            move-to-workspace-12 = mkEmptyArray type.string;
            move-to-workspace-left = mkEmptyArray type.string;
            move-to-workspace-right = mkEmptyArray type.string;
            move-to-workspace-up = mkEmptyArray type.string;
            move-to-workspace-down = mkEmptyArray type.string;
            move-to-workspace-last = mkEmptyArray type.string;
            switch-to-workspace-1 = mkEmptyArray type.string;
            switch-to-workspace-2 = mkEmptyArray type.string;
            switch-to-workspace-3 = mkEmptyArray type.string;
            switch-to-workspace-4 = mkEmptyArray type.string;
            switch-to-workspace-5 = mkEmptyArray type.string;
            switch-to-workspace-6 = mkEmptyArray type.string;
            switch-to-workspace-7 = mkEmptyArray type.string;
            switch-to-workspace-8 = mkEmptyArray type.string;
            switch-to-workspace-9 = mkEmptyArray type.string;
            switch-to-workspace-10 = mkEmptyArray type.string;
            switch-to-workspace-11 = mkEmptyArray type.string;
            switch-to-workspace-12 = mkEmptyArray type.string;
            switch-to-workspace-left = mkEmptyArray type.string;
            switch-to-workspace-right = mkEmptyArray type.string;
            switch-to-workspace-up = mkEmptyArray type.string;
            switch-to-workspace-down = mkEmptyArray type.string;
            switch-to-workspace-last = mkEmptyArray type.string;
          };

          "org/gnome/desktop/wm/preferences" = {
            num-workspaces = mkInt32 1;
          };

          "org/gnome/file-roller/listing" = {
            list-mode = "as-folder";
            show-path = false;
            sort-method = "name";
            sort-type = "ascending";
          };

          "org/gnome/nautilus/list-view" = {
            default-zoom-level = "small";
          };

          "org/gnome/nautilus/preferences" = {
            default-folder-viewer = "list-view";
          };

          "org/gnome/settings-daemon/plugins/media-keys" = {
            custom-keybindings = [
              "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/"
            ];
            home = [ "<Super>e" ];
            magnifier = [ "<Alt><Super>8" ];
            magnifier-zoom-in = [ "<Alt><Super>equal" ];
            magnifier-zoom-out = [ "<Alt><Super>minus" ];
            mic-mute = [ "<Shift><Control><Super>m" ];
            next = [ "<Ctrl><Super>Right" ];
            play = [ "<Ctrl><Super>space" ];
            playback-forward = [ "<Shift><Ctrl><Super>Right" ];
            playback-rewind = [ "<Shift><Ctrl><Super>Left" ];
            previous = [ "<Ctrl><Super>Left" ];
            screensaver = [ "<Super>l" ];
            volume-down-precise = [ "<Ctrl><Super>Down" ];
            volume-mute = [ "<Ctrl><Super>m" ];
            volume-up-precise = [ "<Ctrl><Super>Up" ];
          };

          "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0" = {
            binding = "<Ctrl><Alt>t";
            command = "kgx";
            name = "GNOME Console";
          };

          "org/gnome/shell" = {
            # Get extension IDs from `gnome-extensions list`
            enabled-extensions = [
              "emoji-copy@felipeftn"
              "world_clock_lite@ailin.nemui"
            ];
            favorite-apps = [
              "org.gnome.Console.desktop"
              config.programs.firefox.package.desktopItem.name
              pkgs.obsidian.desktopItem.name
              "org.gnome.Nautilus.desktop"
              nixosConfig.programs.thunderbird.package.desktopItem.name
              "org.telegram.desktop.desktop"
              pkgs.element-desktop.desktopItem.name
              pkgs.discord.desktopItem.name
              "org.keepassxc.KeePassXC.desktop"
              pkgs.jetbrains.idea-community.desktopItem.name
              "org.darktable.darktable.desktop"
              "org.kde.digikam.desktop"
              "spotify.desktop"
              "calibre-gui.desktop"
              "org.qbittorrent.qBittorrent.desktop"
            ];
          };

          "org/gnome/shell/extensions/world-clock" = {
            active-buttons = [
              "@UTC"
              "EGWU"
              "ENGM"
              "YSSY"
              "NZAA"
            ];
          };

          "org/gnome/shell/weather" = {
            automatic-location = true;
            locations = [
              (mkVariant (mkTuple [
                (mkUint32 2)
                (mkVariant (mkTuple [
                  "Zürich"
                  "LSZH"
                  true
                  [
                    (mkTuple [
                      (mkDouble "0.8287405006708767")
                      (mkDouble "0.1489347570190853")
                    ])
                  ]
                  [
                    (mkTuple [
                      (mkDouble "0.8267042948457449")
                      (mkDouble "0.1492256510455152")
                    ])
                  ]
                ]))
              ]))
              (mkVariant (mkTuple [
                (mkUint32 2)
                (mkVariant (mkTuple [
                  "Wellington"
                  "NZWN"
                  true
                  [
                    (mkTuple [
                      (mkDouble "-0.7214027516732254")
                      (mkDouble "3.0508355324860883")
                    ])
                  ]
                  [
                    (mkTuple [
                      (mkDouble "-0.720820981073658")
                      (mkDouble "3.050544638459658")
                    ])
                  ]
                ]))
              ]))
            ];
          };

          "org/gnome/shell/window-switcher" = {
            current-workspace-only = true;
          };

          "org/gnome/shell/world-clocks" = {
            inherit locations;
          };

          "org/gnome/simple-scan" = {
            save-directory = "file://${config.home.homeDirectory}/documents/";
          };

          "org/gnome/software" = {
            first-run = false;
          };

          "org/gnome/tweaks" = {
            show-extensions-notice = false;
          };

          "org/gtk/settings/file-chooser" = {
            show-hidden = false;
            sort-directories-first = true;
            sort-order = "ascending";
          };
        };
      home = {
        file = {
          ".XCompose".source = ../includes/.XCompose;
          ".bash_history" = editableSymlink ".bash_history";
          ".editorconfig".source = ../includes/.editorconfig;
          "idea.properties".text = "idea.filewatcher.executable.path = ${pkgs.fsnotifier}/bin/fsnotifier";
        };
        stateVersion = "24.05";
      };
      programs = {
        git = {
          aliases = {
            authors = "shortlog --email --summary"; # List contributors ordered by name
            current-branch = "symbolic-ref --short HEAD"; # Print currently checked-out branch
            default-remote = "config --default=origin checkout.defaultRemote"; # Print default remote name
            local-only-log = "log --branches --not --remotes --simplify-by-decoration --decorate --oneline"; # List commits not on any remote
            st = "status";
          };
          attributes = [
            "*.bash diff=bash"
            "*.htm diff=html"
            "*.html diff=html"
            "*.java diff=java"
            "*.jpeg diff=jpeg"
            "*.jpg diff=jpeg"
            "*.js diff=javascript"
            "*.markdown diff=markdown"
            "*.md diff=markdown"
            "*.odp diff=odt"
            "*.ods diff=odt"
            "*.odt diff=odt"
            "*.ott diff=odt"
            "*.pdf diff=pdf"
            "*.php diff=php"
            "*.pl diff=perl"
            "*.py diff=python"
            "*.rake diff=ruby"
            "*.rb diff=ruby"
            "*.rs diff=rust"
            "*.tex diff=tex"
            "*.ts diff=javascript"
            "*.xhtml diff=html"
            "GraphicsConfig.xml diff=utf16le"
          ];
          enable = true;
          extraConfig = {
            branch = {
              autoSetupMerge = "simple";
              autoSetupRebase = "always";
              sort = "-committerdate";
            };
            commit = {
              gpgsign = true;
              verbose = true;
            };
            core.untrackedCache = true;
            diff = {
              algorithm = "histogram";
              colorMoved = "plain";
              mnemonicPrefix = true;
              "jpeg" = {
                cachetextconv = true;
                textconv = "exif";
              };
              "odt".textconv = "odt2txt";
              "pdf".command = ''diffpdf "$LOCAL" "$REMOTE"'';
              "utf16le".textconv = "iconv --from-code=utf-16 --to-code=utf-8";
              "sops-decrypt".textconv = "sops decrypt";
            };
            difftool."idea".cmd = ''idea-community diff "$LOCAL" "$REMOTE"'';
            fetch = {
              prune = true;
              pruneTags = true;
            };
            gui = {
              diffContext = 3;
              usettk = 0;
            };
            init.defaultBranch = "main";
            log.date = "iso";
            merge = {
              conflictstyle = "zdiff3";
              keepbackup = false;
              tool = "idea";
            };
            mergetool."idea" = {
              cmd = ''idea-community merge "$LOCAL" "$REMOTE" "$BASE" "$MERGED"'';
              trustExitCode = true;
            };
            pull.rebase = true;
            push = {
              autoSetupRemote = true;
              default = "current";
              followTags = true;
            };
            rebase = {
              autosquash = true;
              autostash = true;
            };
            rerere = {
              # "Reuse recorded resolution of conflicted merges"
              autoUpdate = true;
              enabled = true;
            };
            tag.sort = "version:refname";
            user.signingkey = builtins.toString ../includes/id_ed25519.pub;
          };
          ignores = [
            ".idea/"
            "/.attach_pid*"
          ];
          package = import ../overrides/git-extras {
            package = pkgs.git.override {
              guiSupport = true;
            };
          };
          signing = {
            format = "ssh";
            key = builtins.toString (
              pkgs.writeText "allowed_signers" ''
                ${config.programs.git.userEmail} namespaces="git" ${lib.removeSuffix "\n" (builtins.readFile config.programs.git.extraConfig.user.signingkey)}
              ''
            );
            signByDefault = true;
          };
          userEmail = "${config.home.username}@${nixosConfig.networking.domain}";
          userName = nixosConfig.users.users.victor.description;
        };
        readline = {
          bindings = {
            # Up and down arrows search through the history for the characters before the cursor
            "\\e[A" = "history-search-backward";
            "\\e[B" = "history-search-forward";
          };
          enable = true;
          variables = {
            colored-completion-prefix = true; # Enable coloured highlighting of completions
            completion-ignore-case = true; # Auto-complete files with the wrong case
            completion-query-items = 350; # Ask if completion finds more items than this
            match-hidden-files = false;
            page-completions = false;
            revert-all-at-newline = true; # Don't save edited commands until run
            show-all-if-ambiguous = true;
            visible-stats = true; # Show extra information when completing
          };
        };
      };
      xdg.configFile = lib.attrsets.genAttrs [
        "darktable/darktablerc"
        "darktable/keyboardrc"
        "darktable/shortcutsrc"
        "darktable/shortcutsrc.defaults"
        "darktable/styles/Canon-7D.dtstyle"
        "darktable/styles/Enhanced-color-profile-on-Rec2020-display.dtstyle"
        "digikam_systemrc"
        "digikamrc"
        "keepassxc/keepassxc.ini"
        "mcomix/keybindings.conf"
        "mcomix/preferences.conf"
      ] editableSymlink;
    };
}
