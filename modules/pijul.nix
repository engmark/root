{
  home-manager.users.victor =
    {
      config,
      nixosConfig,
      ...
    }:
    {
      home.file.".config/pijul/config.toml".text = ''
        [author]
        name = "l0b0"
        full_name = "${nixosConfig.users.users.victor.description}"
        email = "${config.programs.git.userEmail}"
        key_path = "${config.home.homeDirectory}/.ssh/id_ed25519.pub"
      '';
    };
}
